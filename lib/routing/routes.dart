import 'package:album/view/home/home_screen.dart';
import 'package:album/view/initial.dart';
import 'package:go_router/go_router.dart';

final List<GoRoute> routes = [
  initRoute,
  homeRoute,
];

// Init
final initRoute = GoRoute(
  path: '/',
  builder: (context, state) => const InitialScreen(),
);

final homeRoute = GoRoute(
  path: '/home',
  builder: (context, state) {
    return HomeScreen(initialIndex: int.tryParse(state.queryParams['index'] ?? ''));
  },
);
