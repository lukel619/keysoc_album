import 'package:album/routing/routes.dart';
import 'package:album/view/page_not_found_screen.dart';
import 'package:go_router/go_router.dart';

final GoRouter router = GoRouter(
  debugLogDiagnostics: true,
  routes: routes,
  errorBuilder: (context, state) => const PageNotFoundScreen(),
);
