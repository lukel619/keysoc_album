import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';

class PageNotFoundScreen extends StatelessWidget {
  const PageNotFoundScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const Text('An error has occured'),
          TextButton(child: const Text('Confirm'), onPressed: () => context.go('/home')),
        ],
      ),
    );
  }
}
