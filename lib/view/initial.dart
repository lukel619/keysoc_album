import 'package:album/provider_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

class InitialScreen extends ConsumerStatefulWidget {
  const InitialScreen({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _InitialScreenState();
}

class _InitialScreenState extends ConsumerState<InitialScreen> {
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 3), () async {
      await ref.read(ProviderLocator.favouriteProvider.notifier).initial();
      FlutterNativeSplash.remove();
    }).whenComplete(() => context.go('/home'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
