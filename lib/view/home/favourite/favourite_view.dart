import 'package:album/model/home/favourite/saved_album.dart';
import 'package:album/provider_locator.dart';
import 'package:album/shared/typography_utils.dart';
import 'package:album/shared/widget/custom_image_handler.dart';
import 'package:album/view/home/explore/widget/favourite_button.dart';
import 'package:album/view/home/widgets/brightness_toggle.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class FavouriteView extends ConsumerStatefulWidget {
  const FavouriteView({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _FavouriteViewState();
}

class _FavouriteViewState extends ConsumerState<FavouriteView> {
  @override
  Widget build(BuildContext context) {
    final List<SavedAlbum>? albumList = ref.watch(ProviderLocator.favouriteProvider.select<List<SavedAlbum>?>((data) => data.whenOrNull<List<SavedAlbum>?>(data: (albums) => albums)));

    return AnimatedSwitcher(
      duration: const Duration(seconds: 1),
      child: Scaffold(
        appBar: AppBar(
          centerTitle: false,
          title: Text(
            'All Saved Albums',
            style: context.displaySmall?.copyWith(fontSize: 24.0),
          ),
          actions: const [BrightnessToggle()],
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(height: 8.0),
                  if (albumList != null)
                    ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: albumList.length,
                      itemBuilder: (context, index) {
                        final item = albumList[index];
                        return Padding(
                          padding: EdgeInsets.only(bottom: index == albumList.length - 1 ? 0 : 12.0),
                          child: SizedBox(
                            height: 175.0,
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                SizedBox(
                                  width: 175.0,
                                  height: 175.0,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(12.0),
                                    child: CustomImageHandler(imagePath: item.artworkUrl100, fit: BoxFit.cover),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 4.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Text(
                                          item.collectionName,
                                          style: context.displaySmall?.copyWith(
                                            fontSize: 16.0,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        const SizedBox(height: 4.0),
                                        Text(
                                          item.artistName,
                                          style: context.displaySmall?.copyWith(
                                            fontSize: 14.0,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          textAlign: TextAlign.left,
                                          maxLines: 1,
                                        ),
                                        const SizedBox(height: 4.0),
                                        Text(
                                          DateTime.parse(item.releaseDate).year.toString(),
                                          style: context.displaySmall?.copyWith(
                                            fontSize: 16.0,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        Expanded(child: Container()),
                                        Row(
                                          children: [
                                            Expanded(child: Container()),
                                            FavouriteButton(
                                              collectionId: item.collectionId,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
