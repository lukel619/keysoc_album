import 'package:album/provider_locator.dart';
import 'package:album/state/home/home_provider_state.dart';
import 'package:album/view/home/explore/explore_view.dart';
import 'package:album/view/home/favourite/favourite_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeScreen extends ConsumerStatefulWidget {
  final int? initialIndex;
  const HomeScreen({Key? key, this.initialIndex}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> {
  final List<NavigationDestination> destinationList = [
    const NavigationDestination(
      icon: Icon(Icons.explore_outlined),
      selectedIcon: Icon(Icons.explore),
      label: 'explore',
    ),
    const NavigationDestination(
      icon: Icon(Icons.favorite_outline_rounded),
      selectedIcon: Icon(Icons.favorite_rounded),
      label: 'Favourite',
    ),
    const NavigationDestination(
      icon: FaIcon(FontAwesomeIcons.circleUser),
      selectedIcon: FaIcon(FontAwesomeIcons.solidCircleUser),
      label: 'Profile',
    ),
  ];

  @override
  void initState() {
    Future.delayed(const Duration(seconds: 0), () {
      ref.read(ProviderLocator.homeProvider.notifier).updateIndex(widget.initialIndex ?? 0);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int selectedIndex = ref.watch(ProviderLocator.homeProvider.select((HomeProviderState value) => value.maybeWhen(data: (data) => data, orElse: () => 0)));

    Widget? view;
    switch (selectedIndex) {
      case 1:
        view = const FavouriteView();
        break;
      case 2:
        view = Scaffold(
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: const [
              Expanded(child: Center(child: Text('Developing'))),
            ],
          ),
        );
        break;
      case 0:
      default:
        view = const ExploreView();
        break;
    }

    return Scaffold(
      body: view,
      bottomNavigationBar: NavigationBar(
        selectedIndex: selectedIndex,
        destinations: destinationList,
        onDestinationSelected: (int index) => ref.read(ProviderLocator.homeProvider.notifier).updateIndex(index),
      ),
    );
  }
}
