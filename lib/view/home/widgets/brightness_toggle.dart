import 'package:album/provider_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class BrightnessToggle extends ConsumerWidget {
  const BrightnessToggle({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final ThemeMode? themeMode = ref.watch(
      ProviderLocator.themeProvider.select(
        (state) => state.mapOrNull(
          system: (settings) => settings.themeSettings.themeMode,
          light: (settings) => settings.themeSettings.themeMode,
          dark: (settings) => settings.themeSettings.themeMode,
        ),
      ),
    );

    return IconButton(
      icon: themeMode == ThemeMode.light ? const Icon(Icons.brightness_3) : const Icon(Icons.brightness_7),
      onPressed: () => ref.read(ProviderLocator.themeProvider.notifier).updateBrightness(),
    );
  }
}
