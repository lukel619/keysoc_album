import 'package:album/model/home/explore/album_list.dart';
import 'package:album/provider_locator.dart';
import 'package:album/shared/typography_utils.dart';
import 'package:album/shared/widget/custom_image_handler.dart';
import 'package:album/view/home/explore/widget/favourite_button.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AllAlbumSection extends ConsumerWidget {
  final ThemeMode? themeMode;
  const AllAlbumSection({super.key, this.themeMode});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final AlbumList? albumList = ref.watch(ProviderLocator.exploreProvider.select((data) => data.whenOrNull(data: (album, profileLink) => album)));

    if (albumList == null || albumList.album.isEmpty) {
      return Container(
        child: Text(
          'No Record found',
          style: context.displaySmall?.copyWith(fontSize: 16.0),
          textAlign: TextAlign.left,
        ),
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          'All Albums',
          style: context.displaySmall?.copyWith(fontSize: 24.0),
          textAlign: TextAlign.left,
        ),
        const SizedBox(height: 8.0),
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: albumList.album.length,
          itemBuilder: (context, index) {
            final item = albumList.album[index];
            return Padding(
              padding: EdgeInsets.only(bottom: index == albumList.album.length - 1 ? 0 : 12.0),
              child: SizedBox(
                height: 175.0,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    SizedBox(
                      width: 175.0,
                      height: 175.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12.0),
                        child: CustomImageHandler(imagePath: item.artworkUrl100, fit: BoxFit.cover),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              item.collectionName,
                              style: context.displaySmall?.copyWith(
                                fontSize: 16.0,
                              ),
                              textAlign: TextAlign.left,
                            ),
                            const SizedBox(height: 4.0),
                            Text(
                              DateTime.parse(item.releaseDate).year.toString(),
                              style: context.displaySmall?.copyWith(
                                fontSize: 16.0,
                                overflow: TextOverflow.ellipsis,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 1,
                            ),
                            Expanded(child: Container()),
                            Row(
                              children: [
                                Expanded(child: Container()),
                                FavouriteButton(
                                  collectionId: item.collectionId,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
