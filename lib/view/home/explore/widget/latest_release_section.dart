import 'package:album/model/home/explore/album.dart';
import 'package:album/provider_locator.dart';
import 'package:album/shared/typography_utils.dart';
import 'package:album/shared/widget/custom_image_handler.dart';
import 'package:album/shared/widget/custom_scroll_row.dart';
import 'package:album/view/home/explore/widget/favourite_button.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class LatestReleaseSection extends ConsumerWidget {
  final ThemeMode? themeMode;
  const LatestReleaseSection({super.key, this.themeMode});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final List<Album>? latestRelease = ref.read(ProviderLocator.exploreProvider.notifier).getLatestList();

    if (latestRelease == null) return Container();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            'Latest Release',
            style: context.displaySmall?.copyWith(fontSize: 24.0),
            textAlign: TextAlign.left,
          ),
        ),
        SizedBox(
          height: 230.0,
          child: CustomScrollingRow(
            axis: Axis.horizontal,
            itemCount: latestRelease.length,
            itemBuilder: (context, i) {
              final Album item = latestRelease[i];
              return Padding(
                padding: EdgeInsets.only(
                  right: i == latestRelease.length - 1 ? 0 : 12.0,
                ),
                child: SizedBox(
                  width: 175.0,
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          SizedBox(
                            width: 175.0,
                            height: 175.0,
                            child: Neumorphic(
                              style: NeumorphicStyle(
                                shape: NeumorphicShape.flat,
                                boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(12)),
                                depth: themeMode != ThemeMode.light ? 3 : 0,
                                lightSource: LightSource.bottomRight,
                                color: Colors.grey,
                              ),
                              child: CustomImageHandler(imagePath: item.artworkUrl100, fit: BoxFit.cover),
                            ),
                          ),
                          Positioned(
                            bottom: 8.0,
                            right: 8.0,
                            child: FavouriteButton(collectionId: item.collectionId),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 2.0),
                        child: Text(
                          item.collectionName,
                          maxLines: 2,
                          textAlign: TextAlign.center,
                          style: context.displaySmall?.copyWith(
                            fontSize: 16.0,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
