import 'package:album/provider_locator.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class FavouriteButton extends ConsumerWidget {
  final int collectionId;

  const FavouriteButton({
    super.key,
    required this.collectionId,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final ThemeMode? themeMode = ref.watch(
      ProviderLocator.themeProvider.select(
        (state) => state.mapOrNull(
          system: (settings) => settings.themeSettings.themeMode,
          light: (settings) => settings.themeSettings.themeMode,
          dark: (settings) => settings.themeSettings.themeMode,
        ),
      ),
    );
    ref.watch(ProviderLocator.favouriteProvider);
    bool isFavourite = ref.read(ProviderLocator.favouriteProvider.notifier).checkFavourite(collectionId);

    return InkWell(
      onTap: () async {
        ref.read(ProviderLocator.favouriteProvider.notifier).updateSavedAlbum(collectionId);
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Updated Favourite'), duration: Duration(seconds: 0)));
      },
      child: NeumorphicIcon(
        isFavourite ? Icons.favorite : Icons.favorite_border,
        style: NeumorphicStyle(
          color: isFavourite
              ? Colors.red
              : themeMode == ThemeMode.light
                  ? Colors.black
                  : Colors.white,
          lightSource: LightSource.bottomRight,
        ),
      ),
    );
  }
}
