import 'package:album/provider_locator.dart';
import 'package:album/shared/typography_utils.dart';
import 'package:album/shared/widget/custom_image_handler.dart';
import 'package:album/view/home/explore/widget/all_album_section.dart';
import 'package:album/view/home/explore/widget/latest_release_section.dart';
import 'package:album/view/home/widgets/brightness_toggle.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class ExploreView extends ConsumerStatefulWidget {
  const ExploreView({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _ExploreViewState();
}

class _ExploreViewState extends ConsumerState<ExploreView> {
  late final TextEditingController searchFieldCtr;
  bool showSuffix = false;

  @override
  void initState() {
    searchFieldCtr = TextEditingController();
    searchFieldCtr.addListener(() {
      if (searchFieldCtr.text.isEmpty) {
        showSuffix = false;
      } else {
        showSuffix = true;
      }
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeMode? themeMode = ref.watch(
      ProviderLocator.themeProvider.select(
        (state) => state.mapOrNull(
          system: (settings) => settings.themeSettings.themeMode,
          light: (settings) => settings.themeSettings.themeMode,
          dark: (settings) => settings.themeSettings.themeMode,
        ),
      ),
    );

    final String? profileLink = ref.watch(ProviderLocator.exploreProvider.select<String?>((data) => data.whenOrNull<String?>(data: (album, profileLink) => profileLink)));
    final String welcomeText = ref.read(ProviderLocator.exploreProvider.notifier).greeting();

    return AnimatedSwitcher(
      duration: const Duration(seconds: 1),
      child: Scaffold(
        appBar: AppBar(
          centerTitle: false,
          title: Text(
            welcomeText,
            style: context.displaySmall?.copyWith(fontSize: 24.0),
          ),
          actions: const [BrightnessToggle()],
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  TextFormField(
                    controller: searchFieldCtr,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Type the artist name',
                      contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                      suffix: showSuffix
                          ? InkWell(
                              onTap: () {
                                searchFieldCtr.clear();
                                ref.read(ProviderLocator.exploreProvider.notifier).reset();
                              },
                              child: const Icon(Icons.close, size: 16),
                            )
                          : null,
                    ),
                    onFieldSubmitted: (value) => ref.read(ProviderLocator.exploreProvider.notifier).fetch(value),
                  ),
                  ref.watch(ProviderLocator.exploreProvider).maybeWhen(
                        loading: () => const Padding(padding: EdgeInsets.only(top: 24.0), child: Center(child: CircularProgressIndicator())),
                        error: () => Text(
                          'Please try again or change the search words.',
                          style: context.displaySmall?.copyWith(fontSize: 16.0),
                          textAlign: TextAlign.left,
                        ),
                        orElse: () => const Padding(padding: EdgeInsets.only(top: 24.0), child: Center(child: CircularProgressIndicator())),
                        data: (album, profileImageLink) => Column(
                          children: [
                            // Profile Image
                            Visibility(
                              visible: profileLink != null,
                              child: profileLink != null
                                  ? SizedBox(
                                      width: MediaQuery.of(context).size.width,
                                      height: 200,
                                      child: Neumorphic(
                                        style: NeumorphicStyle(
                                          shape: NeumorphicShape.flat,
                                          boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(12)),
                                          depth: themeMode != ThemeMode.light ? 3 : 0,
                                          lightSource: LightSource.bottomRight,
                                          color: Colors.grey,
                                        ),
                                        child: CustomImageHandler(
                                          imagePath: profileLink,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    )
                                  : Container(),
                            ),
                            LatestReleaseSection(themeMode: themeMode),
                            AllAlbumSection(themeMode: themeMode),
                          ],
                        ),
                      ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
