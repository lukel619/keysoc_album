import 'package:album/app_content.dart';
import 'package:album/logger/logger.dart';
import 'package:flutter/material.dart';
import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  runApp(
    ProviderScope(
      observers: [Logger()],
      child: const AlbumApp(),
    ),
  );
}

class AlbumApp extends ConsumerWidget {
  const AlbumApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DynamicColorBuilder(builder: (ColorScheme? lightDynamic, ColorScheme? darkDynamic) {
      return AppContent(key: UniqueKey(), lightDynamic: lightDynamic, darkDynamic: darkDynamic);
    });
  }
}
