import 'package:album/model/theme/theme_settings.dart';
import 'package:album/provider/theme/theme_provider.dart';
import 'package:album/provider_locator.dart';
import 'package:album/routing/router.dart';
import 'package:album/state/theme/theme_provider_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AppContent extends ConsumerStatefulWidget {
  final ColorScheme? lightDynamic;
  final ColorScheme? darkDynamic;

  const AppContent({
    super.key,
    this.lightDynamic,
    this.darkDynamic,
  });

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _AppContentState();
}

class _AppContentState extends ConsumerState<AppContent> {
  late final ThemeProvider themeProvider;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    themeProvider = ref.read(ProviderLocator.themeProvider.notifier);
    Future.delayed(const Duration(seconds: 0), () {
      themeProvider.init(widget.lightDynamic, widget.darkDynamic);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeProviderState themeState = ref.watch(ProviderLocator.themeProvider);
    ThemeSettings? settings = themeState.maybeWhen(
      system: (ThemeSettings settings) => settings,
      light: (ThemeSettings settings) => settings,
      dark: (ThemeSettings settings) => settings,
      orElse: () => null,
    );

    return MaterialApp.router(
      key: scaffoldKey,
      routeInformationParser: router.routeInformationParser,
      routerDelegate: router.routerDelegate,
      debugShowCheckedModeBanner: false,
      theme: themeProvider.light(settings?.sourceColor),
      darkTheme: themeProvider.dark(settings?.sourceColor),
      themeMode: settings?.themeMode ?? ThemeMode.system,
      builder: (context, child) {
        // Unfocus Handling
        // Ensure textScaleFactor to be 1 in the whole app
        return GestureDetector(
          onTap: () {
            final FocusNode? focus = FocusManager.instance.primaryFocus;
            if (focus != null) focus.unfocus();
          },
          child: MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: child!,
          ),
        );
      },
    );
  }
}
