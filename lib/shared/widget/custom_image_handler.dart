import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CustomImageHandler extends StatelessWidget {
  final String? imagePath;
  final double? width;
  final double? height;
  final BoxFit? fit;
  final Widget? placeholderWidget;

  const CustomImageHandler({
    Key? key,
    this.imagePath,
    this.width,
    this.height,
    this.fit,
    this.placeholderWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imagePath!,
      fit: fit,
      placeholder: (context, url) => placeholderWidget ?? Container(),
      errorWidget: (context, url, error) => Container(),
    );
  }
}
