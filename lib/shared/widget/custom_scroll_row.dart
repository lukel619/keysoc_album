import 'package:flutter/material.dart';

class CustomScrollingRow extends StatelessWidget {
  final ScrollController? ctr;
  final double? height;
  final int itemCount;
  final Widget Function(BuildContext context, int i) itemBuilder;
  final Axis axis;
  final ScrollPhysics? physics;
  final bool? shrinkWrap;

  const CustomScrollingRow({
    Key? key,
    this.ctr,
    this.axis = Axis.horizontal,
    this.height,
    this.physics,
    this.shrinkWrap,
    required this.itemBuilder,
    required this.itemCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: ctr,
      shrinkWrap: shrinkWrap ?? (axis == Axis.horizontal ? false : true),
      itemBuilder: itemBuilder,
      itemCount: itemCount,
      clipBehavior: Clip.none,
      scrollDirection: axis,
      physics: physics ?? (axis == Axis.horizontal ? const AlwaysScrollableScrollPhysics() : const NeverScrollableScrollPhysics()),
    );
  }
}
