import 'package:album/model/theme/custom_color.dart';
import 'package:album/model/theme/theme_settings.dart';
import 'package:album/shared/color.dart';
import 'package:album/shared/no_animation_page_transitions.dart';
import 'package:album/state/theme/theme_provider_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_color_utilities/material_color_utilities.dart';

// setting page transition animation
const pageTransitionsTheme = PageTransitionsTheme(
  builders: <TargetPlatform, PageTransitionsBuilder>{
    TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
    TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
    TargetPlatform.macOS: NoAnimationPageTransitionsBuilder(),
  },
);

// cloning from https://github.com/flutter/codelabs/tree/master/boring_to_beautiful
class ThemeProvider extends StateNotifier<ThemeProviderState> {
  ThemeProvider(this.read)
      : super(
          ThemeProviderState.system(
            themeSettings: ThemeSettings(
              sourceColor: randomColor(),
              themeMode: ThemeMode.dark,
            ),
          ),
        );

  final Reader read;

  void init(ColorScheme? lightDynamic, ColorScheme? darkDynamic) {
    ThemeSettings settings = state.maybeWhen(
      system: (ThemeSettings settings) => settings,
      orElse: () => ThemeSettings(
        sourceColor: randomColor(),
        themeMode: ThemeMode.dark,
      ),
    );
    settings = settings.copyWith(lightDynamic: lightDynamic);
    settings = settings.copyWith(darkDynamic: darkDynamic);
    state = ThemeProviderState.system(themeSettings: settings);
  }

  void updateBrightness() {
    ThemeSettings? settings = state.maybeWhen(
      system: (ThemeSettings settings) => settings,
      light: (ThemeSettings settings) => settings,
      dark: (ThemeSettings settings) => settings,
      orElse: () => null,
    );

    ThemeSettings? newSettings = settings;

    if (settings?.themeMode == ThemeMode.dark) {
      newSettings = newSettings!.copyWith(themeMode: ThemeMode.light);
      state = ThemeProviderState.light(themeSettings: newSettings);
    } else {
      newSettings = newSettings!.copyWith(themeMode: ThemeMode.dark);
      state = ThemeProviderState.dark(themeSettings: newSettings);
    }
  }

  Color custom(CustomColor custom) {
    if (custom.blend) {
      return blend(custom.color);
    } else {
      return custom.color;
    }
  }

  Color blend(Color targetColor) {
    Color? sourceColor = state.maybeWhen(
      system: (ThemeSettings settings) => settings.sourceColor,
      light: (ThemeSettings settings) => settings.sourceColor,
      dark: (ThemeSettings settings) => settings.sourceColor,
      orElse: () => null,
    );
    int sourceValue = sourceColor?.value ?? randomColor().value;
    return Color(Blend.harmonize(targetColor.value, sourceValue));
  }

  Color source(Color? target) {
    Color? sourceColor = state.maybeWhen(
      system: (ThemeSettings settings) => settings.sourceColor,
      light: (ThemeSettings settings) => settings.sourceColor,
      dark: (ThemeSettings settings) => settings.sourceColor,
      orElse: () => null,
    );
    if (target != null) {
      sourceColor = blend(target);
    }
    return sourceColor ?? randomColor();
  }

  ColorScheme colors(Brightness brightness, Color? targetColor) {
    ThemeSettings? settings = state.maybeWhen(
      system: (ThemeSettings settings) => settings,
      light: (ThemeSettings settings) => settings,
      dark: (ThemeSettings settings) => settings,
      orElse: () => null,
    );
    final dynamicPrimary = brightness == Brightness.light ? settings?.lightDynamic?.primary : settings?.darkDynamic?.primary;
    return ColorScheme.fromSeed(
      seedColor: dynamicPrimary ?? source(targetColor),
      brightness: brightness,
    );
  }

  ShapeBorder get shapeMedium => RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      );

  CardTheme cardTheme() {
    return CardTheme(
      elevation: 0,
      shape: shapeMedium,
      clipBehavior: Clip.antiAlias,
    );
  }

  ListTileThemeData listTileTheme(ColorScheme colors) {
    return ListTileThemeData(
      shape: shapeMedium,
      selectedColor: colors.secondary,
    );
  }

  AppBarTheme appBarTheme(ColorScheme colors) {
    return AppBarTheme(
      elevation: 0,
      backgroundColor: colors.surface,
      foregroundColor: colors.onSurface,
    );
  }

  TabBarTheme tabBarTheme(ColorScheme colors) {
    return TabBarTheme(
      labelColor: colors.secondary,
      unselectedLabelColor: colors.onSurfaceVariant,
      indicator: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: colors.secondary,
            width: 2,
          ),
        ),
      ),
    );
  }

  BottomAppBarTheme bottomAppBarTheme(ColorScheme colors) {
    return BottomAppBarTheme(
      color: colors.surface,
      elevation: 0,
    );
  }

  NavigationBarThemeData navigationBarThemeData(ColorScheme colors) {
    return const NavigationBarThemeData(
      elevation: 0,
      labelBehavior: NavigationDestinationLabelBehavior.onlyShowSelected,
    );
  }

  NavigationRailThemeData navigationRailTheme(ColorScheme colors) {
    return const NavigationRailThemeData();
  }

  DrawerThemeData drawerTheme(ColorScheme colors) {
    return DrawerThemeData(
      backgroundColor: colors.surface,
    );
  }

  ThemeData light([Color? targetColor]) {
    final ColorScheme colorsFromSeed = colors(Brightness.light, targetColor);
    return ThemeData.light().copyWith(
      pageTransitionsTheme: pageTransitionsTheme,
      colorScheme: colorsFromSeed,
      appBarTheme: appBarTheme(colorsFromSeed),
      cardTheme: cardTheme(),
      listTileTheme: listTileTheme(colorsFromSeed),
      bottomAppBarTheme: bottomAppBarTheme(colorsFromSeed),
      navigationBarTheme: navigationBarThemeData(colorsFromSeed),
      navigationRailTheme: navigationRailTheme(colorsFromSeed),
      tabBarTheme: tabBarTheme(colorsFromSeed),
      drawerTheme: drawerTheme(colorsFromSeed),
      scaffoldBackgroundColor: colorsFromSeed.background,
      useMaterial3: true,
      extensions: [],
    );
  }

  ThemeData dark([Color? targetColor]) {
    final ColorScheme colorsFromSeed = colors(Brightness.dark, targetColor);
    return ThemeData.dark().copyWith(
      pageTransitionsTheme: pageTransitionsTheme,
      colorScheme: colorsFromSeed,
      appBarTheme: appBarTheme(colorsFromSeed),
      cardTheme: cardTheme(),
      listTileTheme: listTileTheme(colorsFromSeed),
      bottomAppBarTheme: bottomAppBarTheme(colorsFromSeed),
      navigationBarTheme: navigationBarThemeData(colorsFromSeed),
      navigationRailTheme: navigationRailTheme(colorsFromSeed),
      tabBarTheme: tabBarTheme(colorsFromSeed),
      drawerTheme: drawerTheme(colorsFromSeed),
      scaffoldBackgroundColor: colorsFromSeed.background,
      useMaterial3: true,
      extensions: [],
    );
  }
}
