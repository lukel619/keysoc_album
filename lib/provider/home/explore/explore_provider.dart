import 'package:album/model/home/explore/album.dart';
import 'package:album/model/home/explore/album_list.dart';
import 'package:album/provider_locator.dart';
import 'package:album/services/repository/explore/explore_api_impl.dart';
import 'package:album/state/home/explore/explore_provider_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ExploreProvider extends StateNotifier<ExploreProviderState> {
  ExploreProvider(this.read) : super(const ExploreProviderState.initial()) {
    fetch(null);
  }
  final Reader read;
  late final ExploreService services = read(ProviderLocator.exploreServicesProvider);

  String greeting() {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      return 'Good Morning';
    }
    if (hour < 17) {
      return 'Good Afternoon';
    }
    return 'Good Evening';
  }

  Future<void> fetch(String? searchKeyWord) async {
    try {
      state = const ExploreProviderState.loading();
      String termString = searchKeyWord?.trim() ?? 'jack+johnson';
      if (termString == '') termString = 'jack+johnson';
      termString = termString.replaceAll(' ', '+');
      final AlbumList res = await services.getAlbumList(termString: termString);
      String? profileImageLink;
      if (res.album.isNotEmpty) {
        profileImageLink = await services.getArtistArtworkUrl(res.album[0].artistId);
      }
      state = ExploreProviderState.data(
        list: res,
        profileImageLink: profileImageLink,
      );
    } catch (e) {
      debugPrint(e.toString());
      state = const ExploreProviderState.error();
    }
  }

  Future<void> reset() async {
    List<Album>? albums = state.whenOrNull(data: (albumList, profileImageLink) => albumList.album);
    if (albums == null || albums.isEmpty) {
      await fetch('');
    }
  }

  List<Album>? getLatestList() {
    List<Album>? albums = state.whenOrNull(data: (albumList, profileImageLink) => albumList.album);
    if (albums == null || albums.isEmpty) return null;
    List<Album>? latestAlbums = List.from(albums);
    // revese the comparision
    latestAlbums.sort((a, b) => -DateTime.parse(a.releaseDate).compareTo(DateTime.parse(b.releaseDate)));
    return latestAlbums.length > 5 ? latestAlbums.getRange(0, 5).toList() : latestAlbums;
  }
}
