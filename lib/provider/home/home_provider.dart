import 'package:album/state/home/home_provider_state.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class HomeProvider extends StateNotifier<HomeProviderState> {
  HomeProvider(this.read) : super(const HomeProviderState.initial()) {
    initialProvider();
  }
  final Reader read;

  void initialProvider() {
    state = const HomeProviderState.data(selectedIndex: 0);
  }

  void updateIndex(int selectedIndex) {
    int? currentIndex = state.mapOrNull(data: (state) => state.selectedIndex);
    if (currentIndex != selectedIndex) {
      state = HomeProviderState.data(selectedIndex: selectedIndex);
    }
  }
}
