import 'dart:convert';

import 'package:album/model/home/explore/album.dart';
import 'package:album/model/home/explore/album_list.dart';
import 'package:album/model/home/favourite/saved_album.dart';
import 'package:album/provider_locator.dart';
import 'package:album/state/home/favourite/favourite_provider_state.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class FavouriteProvider extends StateNotifier<FavouriteProviderState> {
  FavouriteProvider(this.read) : super(const FavouriteProviderState.initial());
  final Reader read;
  late final FlutterSecureStorage secureStorage;

  Future<void> initial() async {
    secureStorage = read(ProviderLocator.flutterSecureStorageProvider);
    Map<String, String>? savedAlbumsFromStorage = await secureStorage.readAll();
    List<SavedAlbum> savedAlbums = [];
    savedAlbumsFromStorage.forEach((key, value) {
      Map<String, dynamic> json = jsonDecode(value);
      final SavedAlbum album = SavedAlbum.fromJson(json);
      savedAlbums.add(album);
    });
    savedAlbums.sort((a, b) => b.addedDateTime.compareTo(a.addedDateTime));
    state = FavouriteProviderState.data(savedAlbumList: savedAlbums);
  }

  void updateSavedAlbum(int collectionId) {
    List<SavedAlbum> savedAlbums = state.whenOrNull<List<SavedAlbum>?>(data: (data) => data) ?? [];
    final int savedAlbumIndex = savedAlbums.indexWhere((element) => element.collectionId == collectionId);
    bool saveAlbum = savedAlbumIndex == -1;

    if (saveAlbum) {
      final AlbumList? albumList = read(ProviderLocator.exploreProvider).whenOrNull(data: (album, profileLink) => album);
      if (albumList != null) {
        final Album album = albumList.album.firstWhere((element) => element.collectionId == collectionId);
        SavedAlbum newAlbum = SavedAlbum(
          collectionId: album.collectionId,
          artistId: album.artistId,
          artistName: album.artistName,
          collectionName: album.collectionName,
          releaseDate: album.releaseDate,
          artworkUrl100: album.artworkUrl100,
          addedDateTime: DateTime.now(),
        );
        final List<SavedAlbum> newSavedAlbums = List.from(savedAlbums);
        newSavedAlbums.add(newAlbum);
        secureStorage.write(key: 'saved_album_${album.collectionId}', value: jsonEncode(newAlbum.toJson()));
        state = FavouriteProviderState.data(savedAlbumList: newSavedAlbums);
      }
    } else {
      final List<SavedAlbum> newSavedAlbums = List.from(savedAlbums);
      newSavedAlbums.removeAt(savedAlbumIndex);
      secureStorage.delete(key: 'saved_album_${savedAlbums[savedAlbumIndex].collectionId}');
      state = FavouriteProviderState.data(savedAlbumList: newSavedAlbums);
    }
  }

  bool checkFavourite(int collectionId) {
    List<SavedAlbum> savedAlbums = state.whenOrNull<List<SavedAlbum>?>(data: (data) => data) ?? [];
    final int savedAlbumIndex = savedAlbums.indexWhere((element) => element.collectionId == collectionId);
    return savedAlbumIndex != -1;
  }
}
