import 'package:album/provider/home/explore/explore_provider.dart';
import 'package:album/provider/home/favourite.dart/favourite_provider.dart';
import 'package:album/provider/home/home_provider.dart';
import 'package:album/provider/theme/theme_provider.dart';
import 'package:album/services/api_client.dart';
import 'package:album/services/repository/explore/explore_api_impl.dart';
import 'package:album/state/home/explore/explore_provider_state.dart';
import 'package:album/state/home/favourite/favourite_provider_state.dart';
import 'package:album/state/home/home_provider_state.dart';
import 'package:album/state/theme/theme_provider_state.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ProviderLocator {
  // package
  static final flutterSecureStorageProvider = Provider<FlutterSecureStorage>((ref) => const FlutterSecureStorage());

  // view
  static final themeProvider = StateNotifierProvider<ThemeProvider, ThemeProviderState>((ref) => ThemeProvider(ref.read));
  static final homeProvider = StateNotifierProvider<HomeProvider, HomeProviderState>((ref) => HomeProvider(ref.read));
  static final exploreProvider = StateNotifierProvider<ExploreProvider, ExploreProviderState>((ref) => ExploreProvider(ref.read));
  static final favouriteProvider = StateNotifierProvider<FavouriteProvider, FavouriteProviderState>((ref) => FavouriteProvider(ref.read));

  // services
  static final apiClientProvider = Provider<ApiClient>((ref) => ApiClient());
  static final exploreServicesProvider = Provider<ExploreService>((ref) => ExploreAPIImpl(ref.read));
}
