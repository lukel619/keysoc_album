import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'theme_settings.freezed.dart';

@freezed
class ThemeSettings with _$ThemeSettings {
  factory ThemeSettings({
    @Default(Color(0xff005BBB)) Color sourceColor,
    @Default(ThemeMode.system) ThemeMode themeMode,
    ColorScheme? lightDynamic,
    ColorScheme? darkDynamic,
  }) = _ThemeSettings;
}
