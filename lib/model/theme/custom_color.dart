import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'custom_color.freezed.dart';

@freezed
class CustomColor with _$CustomColor {
  factory CustomColor({
    required String name,
    required Color color,
    @Default(true) bool blend,
  }) = _CustomColor;
}
