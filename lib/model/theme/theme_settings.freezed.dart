// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'theme_settings.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ThemeSettings {
  Color get sourceColor => throw _privateConstructorUsedError;
  ThemeMode get themeMode => throw _privateConstructorUsedError;
  ColorScheme? get lightDynamic => throw _privateConstructorUsedError;
  ColorScheme? get darkDynamic => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ThemeSettingsCopyWith<ThemeSettings> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ThemeSettingsCopyWith<$Res> {
  factory $ThemeSettingsCopyWith(
          ThemeSettings value, $Res Function(ThemeSettings) then) =
      _$ThemeSettingsCopyWithImpl<$Res>;
  $Res call(
      {Color sourceColor,
      ThemeMode themeMode,
      ColorScheme? lightDynamic,
      ColorScheme? darkDynamic});
}

/// @nodoc
class _$ThemeSettingsCopyWithImpl<$Res>
    implements $ThemeSettingsCopyWith<$Res> {
  _$ThemeSettingsCopyWithImpl(this._value, this._then);

  final ThemeSettings _value;
  // ignore: unused_field
  final $Res Function(ThemeSettings) _then;

  @override
  $Res call({
    Object? sourceColor = freezed,
    Object? themeMode = freezed,
    Object? lightDynamic = freezed,
    Object? darkDynamic = freezed,
  }) {
    return _then(_value.copyWith(
      sourceColor: sourceColor == freezed
          ? _value.sourceColor
          : sourceColor // ignore: cast_nullable_to_non_nullable
              as Color,
      themeMode: themeMode == freezed
          ? _value.themeMode
          : themeMode // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
      lightDynamic: lightDynamic == freezed
          ? _value.lightDynamic
          : lightDynamic // ignore: cast_nullable_to_non_nullable
              as ColorScheme?,
      darkDynamic: darkDynamic == freezed
          ? _value.darkDynamic
          : darkDynamic // ignore: cast_nullable_to_non_nullable
              as ColorScheme?,
    ));
  }
}

/// @nodoc
abstract class _$$_ThemeSettingsCopyWith<$Res>
    implements $ThemeSettingsCopyWith<$Res> {
  factory _$$_ThemeSettingsCopyWith(
          _$_ThemeSettings value, $Res Function(_$_ThemeSettings) then) =
      __$$_ThemeSettingsCopyWithImpl<$Res>;
  @override
  $Res call(
      {Color sourceColor,
      ThemeMode themeMode,
      ColorScheme? lightDynamic,
      ColorScheme? darkDynamic});
}

/// @nodoc
class __$$_ThemeSettingsCopyWithImpl<$Res>
    extends _$ThemeSettingsCopyWithImpl<$Res>
    implements _$$_ThemeSettingsCopyWith<$Res> {
  __$$_ThemeSettingsCopyWithImpl(
      _$_ThemeSettings _value, $Res Function(_$_ThemeSettings) _then)
      : super(_value, (v) => _then(v as _$_ThemeSettings));

  @override
  _$_ThemeSettings get _value => super._value as _$_ThemeSettings;

  @override
  $Res call({
    Object? sourceColor = freezed,
    Object? themeMode = freezed,
    Object? lightDynamic = freezed,
    Object? darkDynamic = freezed,
  }) {
    return _then(_$_ThemeSettings(
      sourceColor: sourceColor == freezed
          ? _value.sourceColor
          : sourceColor // ignore: cast_nullable_to_non_nullable
              as Color,
      themeMode: themeMode == freezed
          ? _value.themeMode
          : themeMode // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
      lightDynamic: lightDynamic == freezed
          ? _value.lightDynamic
          : lightDynamic // ignore: cast_nullable_to_non_nullable
              as ColorScheme?,
      darkDynamic: darkDynamic == freezed
          ? _value.darkDynamic
          : darkDynamic // ignore: cast_nullable_to_non_nullable
              as ColorScheme?,
    ));
  }
}

/// @nodoc

class _$_ThemeSettings implements _ThemeSettings {
  _$_ThemeSettings(
      {this.sourceColor = const Color(0xff005BBB),
      this.themeMode = ThemeMode.system,
      this.lightDynamic,
      this.darkDynamic});

  @override
  @JsonKey()
  final Color sourceColor;
  @override
  @JsonKey()
  final ThemeMode themeMode;
  @override
  final ColorScheme? lightDynamic;
  @override
  final ColorScheme? darkDynamic;

  @override
  String toString() {
    return 'ThemeSettings(sourceColor: $sourceColor, themeMode: $themeMode, lightDynamic: $lightDynamic, darkDynamic: $darkDynamic)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ThemeSettings &&
            const DeepCollectionEquality()
                .equals(other.sourceColor, sourceColor) &&
            const DeepCollectionEquality().equals(other.themeMode, themeMode) &&
            const DeepCollectionEquality()
                .equals(other.lightDynamic, lightDynamic) &&
            const DeepCollectionEquality()
                .equals(other.darkDynamic, darkDynamic));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(sourceColor),
      const DeepCollectionEquality().hash(themeMode),
      const DeepCollectionEquality().hash(lightDynamic),
      const DeepCollectionEquality().hash(darkDynamic));

  @JsonKey(ignore: true)
  @override
  _$$_ThemeSettingsCopyWith<_$_ThemeSettings> get copyWith =>
      __$$_ThemeSettingsCopyWithImpl<_$_ThemeSettings>(this, _$identity);
}

abstract class _ThemeSettings implements ThemeSettings {
  factory _ThemeSettings(
      {final Color sourceColor,
      final ThemeMode themeMode,
      final ColorScheme? lightDynamic,
      final ColorScheme? darkDynamic}) = _$_ThemeSettings;

  @override
  Color get sourceColor => throw _privateConstructorUsedError;
  @override
  ThemeMode get themeMode => throw _privateConstructorUsedError;
  @override
  ColorScheme? get lightDynamic => throw _privateConstructorUsedError;
  @override
  ColorScheme? get darkDynamic => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_ThemeSettingsCopyWith<_$_ThemeSettings> get copyWith =>
      throw _privateConstructorUsedError;
}
