// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'custom_color.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CustomColor {
  String get name => throw _privateConstructorUsedError;
  Color get color => throw _privateConstructorUsedError;
  bool get blend => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CustomColorCopyWith<CustomColor> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomColorCopyWith<$Res> {
  factory $CustomColorCopyWith(
          CustomColor value, $Res Function(CustomColor) then) =
      _$CustomColorCopyWithImpl<$Res>;
  $Res call({String name, Color color, bool blend});
}

/// @nodoc
class _$CustomColorCopyWithImpl<$Res> implements $CustomColorCopyWith<$Res> {
  _$CustomColorCopyWithImpl(this._value, this._then);

  final CustomColor _value;
  // ignore: unused_field
  final $Res Function(CustomColor) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? color = freezed,
    Object? blend = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as Color,
      blend: blend == freezed
          ? _value.blend
          : blend // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_CustomColorCopyWith<$Res>
    implements $CustomColorCopyWith<$Res> {
  factory _$$_CustomColorCopyWith(
          _$_CustomColor value, $Res Function(_$_CustomColor) then) =
      __$$_CustomColorCopyWithImpl<$Res>;
  @override
  $Res call({String name, Color color, bool blend});
}

/// @nodoc
class __$$_CustomColorCopyWithImpl<$Res> extends _$CustomColorCopyWithImpl<$Res>
    implements _$$_CustomColorCopyWith<$Res> {
  __$$_CustomColorCopyWithImpl(
      _$_CustomColor _value, $Res Function(_$_CustomColor) _then)
      : super(_value, (v) => _then(v as _$_CustomColor));

  @override
  _$_CustomColor get _value => super._value as _$_CustomColor;

  @override
  $Res call({
    Object? name = freezed,
    Object? color = freezed,
    Object? blend = freezed,
  }) {
    return _then(_$_CustomColor(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as Color,
      blend: blend == freezed
          ? _value.blend
          : blend // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_CustomColor implements _CustomColor {
  _$_CustomColor({required this.name, required this.color, this.blend = true});

  @override
  final String name;
  @override
  final Color color;
  @override
  @JsonKey()
  final bool blend;

  @override
  String toString() {
    return 'CustomColor(name: $name, color: $color, blend: $blend)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CustomColor &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.color, color) &&
            const DeepCollectionEquality().equals(other.blend, blend));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(color),
      const DeepCollectionEquality().hash(blend));

  @JsonKey(ignore: true)
  @override
  _$$_CustomColorCopyWith<_$_CustomColor> get copyWith =>
      __$$_CustomColorCopyWithImpl<_$_CustomColor>(this, _$identity);
}

abstract class _CustomColor implements CustomColor {
  factory _CustomColor(
      {required final String name,
      required final Color color,
      final bool blend}) = _$_CustomColor;

  @override
  String get name => throw _privateConstructorUsedError;
  @override
  Color get color => throw _privateConstructorUsedError;
  @override
  bool get blend => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_CustomColorCopyWith<_$_CustomColor> get copyWith =>
      throw _privateConstructorUsedError;
}
