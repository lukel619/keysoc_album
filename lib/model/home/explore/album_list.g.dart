// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'album_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AlbumList _$$_AlbumListFromJson(Map<String, dynamic> json) => _$_AlbumList(
      resultCount: json['resultCount'] as int,
      album: (json['results'] as List<dynamic>)
          .map((e) => Album.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_AlbumListToJson(_$_AlbumList instance) =>
    <String, dynamic>{
      'resultCount': instance.resultCount,
      'results': instance.album,
    };
