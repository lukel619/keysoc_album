import 'package:freezed_annotation/freezed_annotation.dart';

part 'album.freezed.dart';
part 'album.g.dart';

@freezed
class Album with _$Album {
  factory Album({
    @JsonKey(name: 'wrapperType') required String wrapperType,
    @JsonKey(name: 'collectionType') required String collectionType,
    @JsonKey(name: 'artistId') required int artistId,
    @JsonKey(name: 'collectionId') required int collectionId,
    @JsonKey(name: 'amgArtistId') int? amgArtistId,
    @JsonKey(name: 'artistName') required String artistName,
    @JsonKey(name: 'collectionName') required String collectionName,
    @JsonKey(name: 'collectionCensoredName') required String collectionCensoredName,
    @JsonKey(name: 'artistViewUrl') required String artistViewUrl,
    @JsonKey(name: 'collectionViewUrl') required String collectionViewUrl,
    @JsonKey(name: 'artworkUrl60') required String artworkUrl60,
    @JsonKey(name: 'artworkUrl100') required String artworkUrl100,
    @JsonKey(name: 'collectionPrice') double? collectionPrice,
    @JsonKey(name: 'collectionExplicitness') required String collectionExplicitness,
    @JsonKey(name: 'trackCount') required int trackCount,
    @JsonKey(name: 'copyright') required String copyright,
    @JsonKey(name: 'country') required String country,
    @JsonKey(name: 'currency') required String currency,
    @JsonKey(name: 'releaseDate') required String releaseDate,
    @JsonKey(name: 'primaryGenreName') required String primaryGenreName,
  }) = _Album;
  factory Album.fromJson(Map<String, dynamic> json) => _$AlbumFromJson(json);
}
