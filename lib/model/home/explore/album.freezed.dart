// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'album.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Album _$AlbumFromJson(Map<String, dynamic> json) {
  return _Album.fromJson(json);
}

/// @nodoc
mixin _$Album {
  @JsonKey(name: 'wrapperType')
  String get wrapperType => throw _privateConstructorUsedError;
  @JsonKey(name: 'collectionType')
  String get collectionType => throw _privateConstructorUsedError;
  @JsonKey(name: 'artistId')
  int get artistId => throw _privateConstructorUsedError;
  @JsonKey(name: 'collectionId')
  int get collectionId => throw _privateConstructorUsedError;
  @JsonKey(name: 'amgArtistId')
  int? get amgArtistId => throw _privateConstructorUsedError;
  @JsonKey(name: 'artistName')
  String get artistName => throw _privateConstructorUsedError;
  @JsonKey(name: 'collectionName')
  String get collectionName => throw _privateConstructorUsedError;
  @JsonKey(name: 'collectionCensoredName')
  String get collectionCensoredName => throw _privateConstructorUsedError;
  @JsonKey(name: 'artistViewUrl')
  String get artistViewUrl => throw _privateConstructorUsedError;
  @JsonKey(name: 'collectionViewUrl')
  String get collectionViewUrl => throw _privateConstructorUsedError;
  @JsonKey(name: 'artworkUrl60')
  String get artworkUrl60 => throw _privateConstructorUsedError;
  @JsonKey(name: 'artworkUrl100')
  String get artworkUrl100 => throw _privateConstructorUsedError;
  @JsonKey(name: 'collectionPrice')
  double? get collectionPrice => throw _privateConstructorUsedError;
  @JsonKey(name: 'collectionExplicitness')
  String get collectionExplicitness => throw _privateConstructorUsedError;
  @JsonKey(name: 'trackCount')
  int get trackCount => throw _privateConstructorUsedError;
  @JsonKey(name: 'copyright')
  String get copyright => throw _privateConstructorUsedError;
  @JsonKey(name: 'country')
  String get country => throw _privateConstructorUsedError;
  @JsonKey(name: 'currency')
  String get currency => throw _privateConstructorUsedError;
  @JsonKey(name: 'releaseDate')
  String get releaseDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'primaryGenreName')
  String get primaryGenreName => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AlbumCopyWith<Album> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AlbumCopyWith<$Res> {
  factory $AlbumCopyWith(Album value, $Res Function(Album) then) =
      _$AlbumCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'wrapperType') String wrapperType,
      @JsonKey(name: 'collectionType') String collectionType,
      @JsonKey(name: 'artistId') int artistId,
      @JsonKey(name: 'collectionId') int collectionId,
      @JsonKey(name: 'amgArtistId') int? amgArtistId,
      @JsonKey(name: 'artistName') String artistName,
      @JsonKey(name: 'collectionName') String collectionName,
      @JsonKey(name: 'collectionCensoredName') String collectionCensoredName,
      @JsonKey(name: 'artistViewUrl') String artistViewUrl,
      @JsonKey(name: 'collectionViewUrl') String collectionViewUrl,
      @JsonKey(name: 'artworkUrl60') String artworkUrl60,
      @JsonKey(name: 'artworkUrl100') String artworkUrl100,
      @JsonKey(name: 'collectionPrice') double? collectionPrice,
      @JsonKey(name: 'collectionExplicitness') String collectionExplicitness,
      @JsonKey(name: 'trackCount') int trackCount,
      @JsonKey(name: 'copyright') String copyright,
      @JsonKey(name: 'country') String country,
      @JsonKey(name: 'currency') String currency,
      @JsonKey(name: 'releaseDate') String releaseDate,
      @JsonKey(name: 'primaryGenreName') String primaryGenreName});
}

/// @nodoc
class _$AlbumCopyWithImpl<$Res> implements $AlbumCopyWith<$Res> {
  _$AlbumCopyWithImpl(this._value, this._then);

  final Album _value;
  // ignore: unused_field
  final $Res Function(Album) _then;

  @override
  $Res call({
    Object? wrapperType = freezed,
    Object? collectionType = freezed,
    Object? artistId = freezed,
    Object? collectionId = freezed,
    Object? amgArtistId = freezed,
    Object? artistName = freezed,
    Object? collectionName = freezed,
    Object? collectionCensoredName = freezed,
    Object? artistViewUrl = freezed,
    Object? collectionViewUrl = freezed,
    Object? artworkUrl60 = freezed,
    Object? artworkUrl100 = freezed,
    Object? collectionPrice = freezed,
    Object? collectionExplicitness = freezed,
    Object? trackCount = freezed,
    Object? copyright = freezed,
    Object? country = freezed,
    Object? currency = freezed,
    Object? releaseDate = freezed,
    Object? primaryGenreName = freezed,
  }) {
    return _then(_value.copyWith(
      wrapperType: wrapperType == freezed
          ? _value.wrapperType
          : wrapperType // ignore: cast_nullable_to_non_nullable
              as String,
      collectionType: collectionType == freezed
          ? _value.collectionType
          : collectionType // ignore: cast_nullable_to_non_nullable
              as String,
      artistId: artistId == freezed
          ? _value.artistId
          : artistId // ignore: cast_nullable_to_non_nullable
              as int,
      collectionId: collectionId == freezed
          ? _value.collectionId
          : collectionId // ignore: cast_nullable_to_non_nullable
              as int,
      amgArtistId: amgArtistId == freezed
          ? _value.amgArtistId
          : amgArtistId // ignore: cast_nullable_to_non_nullable
              as int?,
      artistName: artistName == freezed
          ? _value.artistName
          : artistName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionName: collectionName == freezed
          ? _value.collectionName
          : collectionName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionCensoredName: collectionCensoredName == freezed
          ? _value.collectionCensoredName
          : collectionCensoredName // ignore: cast_nullable_to_non_nullable
              as String,
      artistViewUrl: artistViewUrl == freezed
          ? _value.artistViewUrl
          : artistViewUrl // ignore: cast_nullable_to_non_nullable
              as String,
      collectionViewUrl: collectionViewUrl == freezed
          ? _value.collectionViewUrl
          : collectionViewUrl // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl60: artworkUrl60 == freezed
          ? _value.artworkUrl60
          : artworkUrl60 // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl100: artworkUrl100 == freezed
          ? _value.artworkUrl100
          : artworkUrl100 // ignore: cast_nullable_to_non_nullable
              as String,
      collectionPrice: collectionPrice == freezed
          ? _value.collectionPrice
          : collectionPrice // ignore: cast_nullable_to_non_nullable
              as double?,
      collectionExplicitness: collectionExplicitness == freezed
          ? _value.collectionExplicitness
          : collectionExplicitness // ignore: cast_nullable_to_non_nullable
              as String,
      trackCount: trackCount == freezed
          ? _value.trackCount
          : trackCount // ignore: cast_nullable_to_non_nullable
              as int,
      copyright: copyright == freezed
          ? _value.copyright
          : copyright // ignore: cast_nullable_to_non_nullable
              as String,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as String,
      primaryGenreName: primaryGenreName == freezed
          ? _value.primaryGenreName
          : primaryGenreName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_AlbumCopyWith<$Res> implements $AlbumCopyWith<$Res> {
  factory _$$_AlbumCopyWith(_$_Album value, $Res Function(_$_Album) then) =
      __$$_AlbumCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'wrapperType') String wrapperType,
      @JsonKey(name: 'collectionType') String collectionType,
      @JsonKey(name: 'artistId') int artistId,
      @JsonKey(name: 'collectionId') int collectionId,
      @JsonKey(name: 'amgArtistId') int? amgArtistId,
      @JsonKey(name: 'artistName') String artistName,
      @JsonKey(name: 'collectionName') String collectionName,
      @JsonKey(name: 'collectionCensoredName') String collectionCensoredName,
      @JsonKey(name: 'artistViewUrl') String artistViewUrl,
      @JsonKey(name: 'collectionViewUrl') String collectionViewUrl,
      @JsonKey(name: 'artworkUrl60') String artworkUrl60,
      @JsonKey(name: 'artworkUrl100') String artworkUrl100,
      @JsonKey(name: 'collectionPrice') double? collectionPrice,
      @JsonKey(name: 'collectionExplicitness') String collectionExplicitness,
      @JsonKey(name: 'trackCount') int trackCount,
      @JsonKey(name: 'copyright') String copyright,
      @JsonKey(name: 'country') String country,
      @JsonKey(name: 'currency') String currency,
      @JsonKey(name: 'releaseDate') String releaseDate,
      @JsonKey(name: 'primaryGenreName') String primaryGenreName});
}

/// @nodoc
class __$$_AlbumCopyWithImpl<$Res> extends _$AlbumCopyWithImpl<$Res>
    implements _$$_AlbumCopyWith<$Res> {
  __$$_AlbumCopyWithImpl(_$_Album _value, $Res Function(_$_Album) _then)
      : super(_value, (v) => _then(v as _$_Album));

  @override
  _$_Album get _value => super._value as _$_Album;

  @override
  $Res call({
    Object? wrapperType = freezed,
    Object? collectionType = freezed,
    Object? artistId = freezed,
    Object? collectionId = freezed,
    Object? amgArtistId = freezed,
    Object? artistName = freezed,
    Object? collectionName = freezed,
    Object? collectionCensoredName = freezed,
    Object? artistViewUrl = freezed,
    Object? collectionViewUrl = freezed,
    Object? artworkUrl60 = freezed,
    Object? artworkUrl100 = freezed,
    Object? collectionPrice = freezed,
    Object? collectionExplicitness = freezed,
    Object? trackCount = freezed,
    Object? copyright = freezed,
    Object? country = freezed,
    Object? currency = freezed,
    Object? releaseDate = freezed,
    Object? primaryGenreName = freezed,
  }) {
    return _then(_$_Album(
      wrapperType: wrapperType == freezed
          ? _value.wrapperType
          : wrapperType // ignore: cast_nullable_to_non_nullable
              as String,
      collectionType: collectionType == freezed
          ? _value.collectionType
          : collectionType // ignore: cast_nullable_to_non_nullable
              as String,
      artistId: artistId == freezed
          ? _value.artistId
          : artistId // ignore: cast_nullable_to_non_nullable
              as int,
      collectionId: collectionId == freezed
          ? _value.collectionId
          : collectionId // ignore: cast_nullable_to_non_nullable
              as int,
      amgArtistId: amgArtistId == freezed
          ? _value.amgArtistId
          : amgArtistId // ignore: cast_nullable_to_non_nullable
              as int?,
      artistName: artistName == freezed
          ? _value.artistName
          : artistName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionName: collectionName == freezed
          ? _value.collectionName
          : collectionName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionCensoredName: collectionCensoredName == freezed
          ? _value.collectionCensoredName
          : collectionCensoredName // ignore: cast_nullable_to_non_nullable
              as String,
      artistViewUrl: artistViewUrl == freezed
          ? _value.artistViewUrl
          : artistViewUrl // ignore: cast_nullable_to_non_nullable
              as String,
      collectionViewUrl: collectionViewUrl == freezed
          ? _value.collectionViewUrl
          : collectionViewUrl // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl60: artworkUrl60 == freezed
          ? _value.artworkUrl60
          : artworkUrl60 // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl100: artworkUrl100 == freezed
          ? _value.artworkUrl100
          : artworkUrl100 // ignore: cast_nullable_to_non_nullable
              as String,
      collectionPrice: collectionPrice == freezed
          ? _value.collectionPrice
          : collectionPrice // ignore: cast_nullable_to_non_nullable
              as double?,
      collectionExplicitness: collectionExplicitness == freezed
          ? _value.collectionExplicitness
          : collectionExplicitness // ignore: cast_nullable_to_non_nullable
              as String,
      trackCount: trackCount == freezed
          ? _value.trackCount
          : trackCount // ignore: cast_nullable_to_non_nullable
              as int,
      copyright: copyright == freezed
          ? _value.copyright
          : copyright // ignore: cast_nullable_to_non_nullable
              as String,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as String,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as String,
      primaryGenreName: primaryGenreName == freezed
          ? _value.primaryGenreName
          : primaryGenreName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Album implements _Album {
  _$_Album(
      {@JsonKey(name: 'wrapperType')
          required this.wrapperType,
      @JsonKey(name: 'collectionType')
          required this.collectionType,
      @JsonKey(name: 'artistId')
          required this.artistId,
      @JsonKey(name: 'collectionId')
          required this.collectionId,
      @JsonKey(name: 'amgArtistId')
          this.amgArtistId,
      @JsonKey(name: 'artistName')
          required this.artistName,
      @JsonKey(name: 'collectionName')
          required this.collectionName,
      @JsonKey(name: 'collectionCensoredName')
          required this.collectionCensoredName,
      @JsonKey(name: 'artistViewUrl')
          required this.artistViewUrl,
      @JsonKey(name: 'collectionViewUrl')
          required this.collectionViewUrl,
      @JsonKey(name: 'artworkUrl60')
          required this.artworkUrl60,
      @JsonKey(name: 'artworkUrl100')
          required this.artworkUrl100,
      @JsonKey(name: 'collectionPrice')
          this.collectionPrice,
      @JsonKey(name: 'collectionExplicitness')
          required this.collectionExplicitness,
      @JsonKey(name: 'trackCount')
          required this.trackCount,
      @JsonKey(name: 'copyright')
          required this.copyright,
      @JsonKey(name: 'country')
          required this.country,
      @JsonKey(name: 'currency')
          required this.currency,
      @JsonKey(name: 'releaseDate')
          required this.releaseDate,
      @JsonKey(name: 'primaryGenreName')
          required this.primaryGenreName});

  factory _$_Album.fromJson(Map<String, dynamic> json) =>
      _$$_AlbumFromJson(json);

  @override
  @JsonKey(name: 'wrapperType')
  final String wrapperType;
  @override
  @JsonKey(name: 'collectionType')
  final String collectionType;
  @override
  @JsonKey(name: 'artistId')
  final int artistId;
  @override
  @JsonKey(name: 'collectionId')
  final int collectionId;
  @override
  @JsonKey(name: 'amgArtistId')
  final int? amgArtistId;
  @override
  @JsonKey(name: 'artistName')
  final String artistName;
  @override
  @JsonKey(name: 'collectionName')
  final String collectionName;
  @override
  @JsonKey(name: 'collectionCensoredName')
  final String collectionCensoredName;
  @override
  @JsonKey(name: 'artistViewUrl')
  final String artistViewUrl;
  @override
  @JsonKey(name: 'collectionViewUrl')
  final String collectionViewUrl;
  @override
  @JsonKey(name: 'artworkUrl60')
  final String artworkUrl60;
  @override
  @JsonKey(name: 'artworkUrl100')
  final String artworkUrl100;
  @override
  @JsonKey(name: 'collectionPrice')
  final double? collectionPrice;
  @override
  @JsonKey(name: 'collectionExplicitness')
  final String collectionExplicitness;
  @override
  @JsonKey(name: 'trackCount')
  final int trackCount;
  @override
  @JsonKey(name: 'copyright')
  final String copyright;
  @override
  @JsonKey(name: 'country')
  final String country;
  @override
  @JsonKey(name: 'currency')
  final String currency;
  @override
  @JsonKey(name: 'releaseDate')
  final String releaseDate;
  @override
  @JsonKey(name: 'primaryGenreName')
  final String primaryGenreName;

  @override
  String toString() {
    return 'Album(wrapperType: $wrapperType, collectionType: $collectionType, artistId: $artistId, collectionId: $collectionId, amgArtistId: $amgArtistId, artistName: $artistName, collectionName: $collectionName, collectionCensoredName: $collectionCensoredName, artistViewUrl: $artistViewUrl, collectionViewUrl: $collectionViewUrl, artworkUrl60: $artworkUrl60, artworkUrl100: $artworkUrl100, collectionPrice: $collectionPrice, collectionExplicitness: $collectionExplicitness, trackCount: $trackCount, copyright: $copyright, country: $country, currency: $currency, releaseDate: $releaseDate, primaryGenreName: $primaryGenreName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Album &&
            const DeepCollectionEquality()
                .equals(other.wrapperType, wrapperType) &&
            const DeepCollectionEquality()
                .equals(other.collectionType, collectionType) &&
            const DeepCollectionEquality().equals(other.artistId, artistId) &&
            const DeepCollectionEquality()
                .equals(other.collectionId, collectionId) &&
            const DeepCollectionEquality()
                .equals(other.amgArtistId, amgArtistId) &&
            const DeepCollectionEquality()
                .equals(other.artistName, artistName) &&
            const DeepCollectionEquality()
                .equals(other.collectionName, collectionName) &&
            const DeepCollectionEquality()
                .equals(other.collectionCensoredName, collectionCensoredName) &&
            const DeepCollectionEquality()
                .equals(other.artistViewUrl, artistViewUrl) &&
            const DeepCollectionEquality()
                .equals(other.collectionViewUrl, collectionViewUrl) &&
            const DeepCollectionEquality()
                .equals(other.artworkUrl60, artworkUrl60) &&
            const DeepCollectionEquality()
                .equals(other.artworkUrl100, artworkUrl100) &&
            const DeepCollectionEquality()
                .equals(other.collectionPrice, collectionPrice) &&
            const DeepCollectionEquality()
                .equals(other.collectionExplicitness, collectionExplicitness) &&
            const DeepCollectionEquality()
                .equals(other.trackCount, trackCount) &&
            const DeepCollectionEquality().equals(other.copyright, copyright) &&
            const DeepCollectionEquality().equals(other.country, country) &&
            const DeepCollectionEquality().equals(other.currency, currency) &&
            const DeepCollectionEquality()
                .equals(other.releaseDate, releaseDate) &&
            const DeepCollectionEquality()
                .equals(other.primaryGenreName, primaryGenreName));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(wrapperType),
        const DeepCollectionEquality().hash(collectionType),
        const DeepCollectionEquality().hash(artistId),
        const DeepCollectionEquality().hash(collectionId),
        const DeepCollectionEquality().hash(amgArtistId),
        const DeepCollectionEquality().hash(artistName),
        const DeepCollectionEquality().hash(collectionName),
        const DeepCollectionEquality().hash(collectionCensoredName),
        const DeepCollectionEquality().hash(artistViewUrl),
        const DeepCollectionEquality().hash(collectionViewUrl),
        const DeepCollectionEquality().hash(artworkUrl60),
        const DeepCollectionEquality().hash(artworkUrl100),
        const DeepCollectionEquality().hash(collectionPrice),
        const DeepCollectionEquality().hash(collectionExplicitness),
        const DeepCollectionEquality().hash(trackCount),
        const DeepCollectionEquality().hash(copyright),
        const DeepCollectionEquality().hash(country),
        const DeepCollectionEquality().hash(currency),
        const DeepCollectionEquality().hash(releaseDate),
        const DeepCollectionEquality().hash(primaryGenreName)
      ]);

  @JsonKey(ignore: true)
  @override
  _$$_AlbumCopyWith<_$_Album> get copyWith =>
      __$$_AlbumCopyWithImpl<_$_Album>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AlbumToJson(this);
  }
}

abstract class _Album implements Album {
  factory _Album(
      {@JsonKey(name: 'wrapperType')
          required final String wrapperType,
      @JsonKey(name: 'collectionType')
          required final String collectionType,
      @JsonKey(name: 'artistId')
          required final int artistId,
      @JsonKey(name: 'collectionId')
          required final int collectionId,
      @JsonKey(name: 'amgArtistId')
          final int? amgArtistId,
      @JsonKey(name: 'artistName')
          required final String artistName,
      @JsonKey(name: 'collectionName')
          required final String collectionName,
      @JsonKey(name: 'collectionCensoredName')
          required final String collectionCensoredName,
      @JsonKey(name: 'artistViewUrl')
          required final String artistViewUrl,
      @JsonKey(name: 'collectionViewUrl')
          required final String collectionViewUrl,
      @JsonKey(name: 'artworkUrl60')
          required final String artworkUrl60,
      @JsonKey(name: 'artworkUrl100')
          required final String artworkUrl100,
      @JsonKey(name: 'collectionPrice')
          final double? collectionPrice,
      @JsonKey(name: 'collectionExplicitness')
          required final String collectionExplicitness,
      @JsonKey(name: 'trackCount')
          required final int trackCount,
      @JsonKey(name: 'copyright')
          required final String copyright,
      @JsonKey(name: 'country')
          required final String country,
      @JsonKey(name: 'currency')
          required final String currency,
      @JsonKey(name: 'releaseDate')
          required final String releaseDate,
      @JsonKey(name: 'primaryGenreName')
          required final String primaryGenreName}) = _$_Album;

  factory _Album.fromJson(Map<String, dynamic> json) = _$_Album.fromJson;

  @override
  @JsonKey(name: 'wrapperType')
  String get wrapperType => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'collectionType')
  String get collectionType => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'artistId')
  int get artistId => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'collectionId')
  int get collectionId => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'amgArtistId')
  int? get amgArtistId => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'artistName')
  String get artistName => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'collectionName')
  String get collectionName => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'collectionCensoredName')
  String get collectionCensoredName => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'artistViewUrl')
  String get artistViewUrl => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'collectionViewUrl')
  String get collectionViewUrl => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'artworkUrl60')
  String get artworkUrl60 => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'artworkUrl100')
  String get artworkUrl100 => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'collectionPrice')
  double? get collectionPrice => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'collectionExplicitness')
  String get collectionExplicitness => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'trackCount')
  int get trackCount => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'copyright')
  String get copyright => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'country')
  String get country => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'currency')
  String get currency => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'releaseDate')
  String get releaseDate => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'primaryGenreName')
  String get primaryGenreName => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_AlbumCopyWith<_$_Album> get copyWith =>
      throw _privateConstructorUsedError;
}
