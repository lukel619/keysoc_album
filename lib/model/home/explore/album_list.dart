import 'package:album/model/home/explore/album.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'album_list.freezed.dart';
part 'album_list.g.dart';

@freezed
class AlbumList with _$AlbumList {
  factory AlbumList({
    @JsonKey(name: 'resultCount') required int resultCount,
    @JsonKey(name: 'results') required List<Album> album,
  }) = _AlbumList;
  factory AlbumList.fromJson(Map<String, dynamic> json) => _$AlbumListFromJson(json);
}
