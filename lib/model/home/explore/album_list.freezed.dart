// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'album_list.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AlbumList _$AlbumListFromJson(Map<String, dynamic> json) {
  return _AlbumList.fromJson(json);
}

/// @nodoc
mixin _$AlbumList {
  @JsonKey(name: 'resultCount')
  int get resultCount => throw _privateConstructorUsedError;
  @JsonKey(name: 'results')
  List<Album> get album => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AlbumListCopyWith<AlbumList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AlbumListCopyWith<$Res> {
  factory $AlbumListCopyWith(AlbumList value, $Res Function(AlbumList) then) =
      _$AlbumListCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'resultCount') int resultCount,
      @JsonKey(name: 'results') List<Album> album});
}

/// @nodoc
class _$AlbumListCopyWithImpl<$Res> implements $AlbumListCopyWith<$Res> {
  _$AlbumListCopyWithImpl(this._value, this._then);

  final AlbumList _value;
  // ignore: unused_field
  final $Res Function(AlbumList) _then;

  @override
  $Res call({
    Object? resultCount = freezed,
    Object? album = freezed,
  }) {
    return _then(_value.copyWith(
      resultCount: resultCount == freezed
          ? _value.resultCount
          : resultCount // ignore: cast_nullable_to_non_nullable
              as int,
      album: album == freezed
          ? _value.album
          : album // ignore: cast_nullable_to_non_nullable
              as List<Album>,
    ));
  }
}

/// @nodoc
abstract class _$$_AlbumListCopyWith<$Res> implements $AlbumListCopyWith<$Res> {
  factory _$$_AlbumListCopyWith(
          _$_AlbumList value, $Res Function(_$_AlbumList) then) =
      __$$_AlbumListCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'resultCount') int resultCount,
      @JsonKey(name: 'results') List<Album> album});
}

/// @nodoc
class __$$_AlbumListCopyWithImpl<$Res> extends _$AlbumListCopyWithImpl<$Res>
    implements _$$_AlbumListCopyWith<$Res> {
  __$$_AlbumListCopyWithImpl(
      _$_AlbumList _value, $Res Function(_$_AlbumList) _then)
      : super(_value, (v) => _then(v as _$_AlbumList));

  @override
  _$_AlbumList get _value => super._value as _$_AlbumList;

  @override
  $Res call({
    Object? resultCount = freezed,
    Object? album = freezed,
  }) {
    return _then(_$_AlbumList(
      resultCount: resultCount == freezed
          ? _value.resultCount
          : resultCount // ignore: cast_nullable_to_non_nullable
              as int,
      album: album == freezed
          ? _value._album
          : album // ignore: cast_nullable_to_non_nullable
              as List<Album>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AlbumList implements _AlbumList {
  _$_AlbumList(
      {@JsonKey(name: 'resultCount') required this.resultCount,
      @JsonKey(name: 'results') required final List<Album> album})
      : _album = album;

  factory _$_AlbumList.fromJson(Map<String, dynamic> json) =>
      _$$_AlbumListFromJson(json);

  @override
  @JsonKey(name: 'resultCount')
  final int resultCount;
  final List<Album> _album;
  @override
  @JsonKey(name: 'results')
  List<Album> get album {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_album);
  }

  @override
  String toString() {
    return 'AlbumList(resultCount: $resultCount, album: $album)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AlbumList &&
            const DeepCollectionEquality()
                .equals(other.resultCount, resultCount) &&
            const DeepCollectionEquality().equals(other._album, _album));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(resultCount),
      const DeepCollectionEquality().hash(_album));

  @JsonKey(ignore: true)
  @override
  _$$_AlbumListCopyWith<_$_AlbumList> get copyWith =>
      __$$_AlbumListCopyWithImpl<_$_AlbumList>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AlbumListToJson(this);
  }
}

abstract class _AlbumList implements AlbumList {
  factory _AlbumList(
          {@JsonKey(name: 'resultCount') required final int resultCount,
          @JsonKey(name: 'results') required final List<Album> album}) =
      _$_AlbumList;

  factory _AlbumList.fromJson(Map<String, dynamic> json) =
      _$_AlbumList.fromJson;

  @override
  @JsonKey(name: 'resultCount')
  int get resultCount => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'results')
  List<Album> get album => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_AlbumListCopyWith<_$_AlbumList> get copyWith =>
      throw _privateConstructorUsedError;
}
