// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'saved_album.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SavedAlbum _$SavedAlbumFromJson(Map<String, dynamic> json) {
  return _SavedAlbum.fromJson(json);
}

/// @nodoc
mixin _$SavedAlbum {
  @JsonKey(name: 'artistId')
  int get artistId => throw _privateConstructorUsedError;
  @JsonKey(name: 'artistName')
  String get artistName => throw _privateConstructorUsedError;
  @JsonKey(name: 'collectionId')
  int get collectionId => throw _privateConstructorUsedError;
  @JsonKey(name: 'collectionName')
  String get collectionName => throw _privateConstructorUsedError;
  @JsonKey(name: 'releaseDate')
  String get releaseDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'artworkUrl100')
  String get artworkUrl100 => throw _privateConstructorUsedError;
  @JsonKey(name: 'addedDateTime')
  DateTime get addedDateTime => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SavedAlbumCopyWith<SavedAlbum> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SavedAlbumCopyWith<$Res> {
  factory $SavedAlbumCopyWith(
          SavedAlbum value, $Res Function(SavedAlbum) then) =
      _$SavedAlbumCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'artistId') int artistId,
      @JsonKey(name: 'artistName') String artistName,
      @JsonKey(name: 'collectionId') int collectionId,
      @JsonKey(name: 'collectionName') String collectionName,
      @JsonKey(name: 'releaseDate') String releaseDate,
      @JsonKey(name: 'artworkUrl100') String artworkUrl100,
      @JsonKey(name: 'addedDateTime') DateTime addedDateTime});
}

/// @nodoc
class _$SavedAlbumCopyWithImpl<$Res> implements $SavedAlbumCopyWith<$Res> {
  _$SavedAlbumCopyWithImpl(this._value, this._then);

  final SavedAlbum _value;
  // ignore: unused_field
  final $Res Function(SavedAlbum) _then;

  @override
  $Res call({
    Object? artistId = freezed,
    Object? artistName = freezed,
    Object? collectionId = freezed,
    Object? collectionName = freezed,
    Object? releaseDate = freezed,
    Object? artworkUrl100 = freezed,
    Object? addedDateTime = freezed,
  }) {
    return _then(_value.copyWith(
      artistId: artistId == freezed
          ? _value.artistId
          : artistId // ignore: cast_nullable_to_non_nullable
              as int,
      artistName: artistName == freezed
          ? _value.artistName
          : artistName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionId: collectionId == freezed
          ? _value.collectionId
          : collectionId // ignore: cast_nullable_to_non_nullable
              as int,
      collectionName: collectionName == freezed
          ? _value.collectionName
          : collectionName // ignore: cast_nullable_to_non_nullable
              as String,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl100: artworkUrl100 == freezed
          ? _value.artworkUrl100
          : artworkUrl100 // ignore: cast_nullable_to_non_nullable
              as String,
      addedDateTime: addedDateTime == freezed
          ? _value.addedDateTime
          : addedDateTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$$_SavedAlbumCopyWith<$Res>
    implements $SavedAlbumCopyWith<$Res> {
  factory _$$_SavedAlbumCopyWith(
          _$_SavedAlbum value, $Res Function(_$_SavedAlbum) then) =
      __$$_SavedAlbumCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'artistId') int artistId,
      @JsonKey(name: 'artistName') String artistName,
      @JsonKey(name: 'collectionId') int collectionId,
      @JsonKey(name: 'collectionName') String collectionName,
      @JsonKey(name: 'releaseDate') String releaseDate,
      @JsonKey(name: 'artworkUrl100') String artworkUrl100,
      @JsonKey(name: 'addedDateTime') DateTime addedDateTime});
}

/// @nodoc
class __$$_SavedAlbumCopyWithImpl<$Res> extends _$SavedAlbumCopyWithImpl<$Res>
    implements _$$_SavedAlbumCopyWith<$Res> {
  __$$_SavedAlbumCopyWithImpl(
      _$_SavedAlbum _value, $Res Function(_$_SavedAlbum) _then)
      : super(_value, (v) => _then(v as _$_SavedAlbum));

  @override
  _$_SavedAlbum get _value => super._value as _$_SavedAlbum;

  @override
  $Res call({
    Object? artistId = freezed,
    Object? artistName = freezed,
    Object? collectionId = freezed,
    Object? collectionName = freezed,
    Object? releaseDate = freezed,
    Object? artworkUrl100 = freezed,
    Object? addedDateTime = freezed,
  }) {
    return _then(_$_SavedAlbum(
      artistId: artistId == freezed
          ? _value.artistId
          : artistId // ignore: cast_nullable_to_non_nullable
              as int,
      artistName: artistName == freezed
          ? _value.artistName
          : artistName // ignore: cast_nullable_to_non_nullable
              as String,
      collectionId: collectionId == freezed
          ? _value.collectionId
          : collectionId // ignore: cast_nullable_to_non_nullable
              as int,
      collectionName: collectionName == freezed
          ? _value.collectionName
          : collectionName // ignore: cast_nullable_to_non_nullable
              as String,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as String,
      artworkUrl100: artworkUrl100 == freezed
          ? _value.artworkUrl100
          : artworkUrl100 // ignore: cast_nullable_to_non_nullable
              as String,
      addedDateTime: addedDateTime == freezed
          ? _value.addedDateTime
          : addedDateTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SavedAlbum implements _SavedAlbum {
  _$_SavedAlbum(
      {@JsonKey(name: 'artistId') required this.artistId,
      @JsonKey(name: 'artistName') required this.artistName,
      @JsonKey(name: 'collectionId') required this.collectionId,
      @JsonKey(name: 'collectionName') required this.collectionName,
      @JsonKey(name: 'releaseDate') required this.releaseDate,
      @JsonKey(name: 'artworkUrl100') required this.artworkUrl100,
      @JsonKey(name: 'addedDateTime') required this.addedDateTime});

  factory _$_SavedAlbum.fromJson(Map<String, dynamic> json) =>
      _$$_SavedAlbumFromJson(json);

  @override
  @JsonKey(name: 'artistId')
  final int artistId;
  @override
  @JsonKey(name: 'artistName')
  final String artistName;
  @override
  @JsonKey(name: 'collectionId')
  final int collectionId;
  @override
  @JsonKey(name: 'collectionName')
  final String collectionName;
  @override
  @JsonKey(name: 'releaseDate')
  final String releaseDate;
  @override
  @JsonKey(name: 'artworkUrl100')
  final String artworkUrl100;
  @override
  @JsonKey(name: 'addedDateTime')
  final DateTime addedDateTime;

  @override
  String toString() {
    return 'SavedAlbum(artistId: $artistId, artistName: $artistName, collectionId: $collectionId, collectionName: $collectionName, releaseDate: $releaseDate, artworkUrl100: $artworkUrl100, addedDateTime: $addedDateTime)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SavedAlbum &&
            const DeepCollectionEquality().equals(other.artistId, artistId) &&
            const DeepCollectionEquality()
                .equals(other.artistName, artistName) &&
            const DeepCollectionEquality()
                .equals(other.collectionId, collectionId) &&
            const DeepCollectionEquality()
                .equals(other.collectionName, collectionName) &&
            const DeepCollectionEquality()
                .equals(other.releaseDate, releaseDate) &&
            const DeepCollectionEquality()
                .equals(other.artworkUrl100, artworkUrl100) &&
            const DeepCollectionEquality()
                .equals(other.addedDateTime, addedDateTime));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(artistId),
      const DeepCollectionEquality().hash(artistName),
      const DeepCollectionEquality().hash(collectionId),
      const DeepCollectionEquality().hash(collectionName),
      const DeepCollectionEquality().hash(releaseDate),
      const DeepCollectionEquality().hash(artworkUrl100),
      const DeepCollectionEquality().hash(addedDateTime));

  @JsonKey(ignore: true)
  @override
  _$$_SavedAlbumCopyWith<_$_SavedAlbum> get copyWith =>
      __$$_SavedAlbumCopyWithImpl<_$_SavedAlbum>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SavedAlbumToJson(this);
  }
}

abstract class _SavedAlbum implements SavedAlbum {
  factory _SavedAlbum(
      {@JsonKey(name: 'artistId')
          required final int artistId,
      @JsonKey(name: 'artistName')
          required final String artistName,
      @JsonKey(name: 'collectionId')
          required final int collectionId,
      @JsonKey(name: 'collectionName')
          required final String collectionName,
      @JsonKey(name: 'releaseDate')
          required final String releaseDate,
      @JsonKey(name: 'artworkUrl100')
          required final String artworkUrl100,
      @JsonKey(name: 'addedDateTime')
          required final DateTime addedDateTime}) = _$_SavedAlbum;

  factory _SavedAlbum.fromJson(Map<String, dynamic> json) =
      _$_SavedAlbum.fromJson;

  @override
  @JsonKey(name: 'artistId')
  int get artistId => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'artistName')
  String get artistName => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'collectionId')
  int get collectionId => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'collectionName')
  String get collectionName => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'releaseDate')
  String get releaseDate => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'artworkUrl100')
  String get artworkUrl100 => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'addedDateTime')
  DateTime get addedDateTime => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_SavedAlbumCopyWith<_$_SavedAlbum> get copyWith =>
      throw _privateConstructorUsedError;
}
