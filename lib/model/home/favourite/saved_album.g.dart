// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'saved_album.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SavedAlbum _$$_SavedAlbumFromJson(Map<String, dynamic> json) =>
    _$_SavedAlbum(
      artistId: json['artistId'] as int,
      artistName: json['artistName'] as String,
      collectionId: json['collectionId'] as int,
      collectionName: json['collectionName'] as String,
      releaseDate: json['releaseDate'] as String,
      artworkUrl100: json['artworkUrl100'] as String,
      addedDateTime: DateTime.parse(json['addedDateTime'] as String),
    );

Map<String, dynamic> _$$_SavedAlbumToJson(_$_SavedAlbum instance) =>
    <String, dynamic>{
      'artistId': instance.artistId,
      'artistName': instance.artistName,
      'collectionId': instance.collectionId,
      'collectionName': instance.collectionName,
      'releaseDate': instance.releaseDate,
      'artworkUrl100': instance.artworkUrl100,
      'addedDateTime': instance.addedDateTime.toIso8601String(),
    };
