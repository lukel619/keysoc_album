import 'package:freezed_annotation/freezed_annotation.dart';

part 'saved_album.freezed.dart';
part 'saved_album.g.dart';

@freezed
class SavedAlbum with _$SavedAlbum {
  factory SavedAlbum({
    @JsonKey(name: 'artistId') required int artistId,
    @JsonKey(name: 'artistName') required String artistName,
    @JsonKey(name: 'collectionId') required int collectionId,
    @JsonKey(name: 'collectionName') required String collectionName,
    @JsonKey(name: 'releaseDate') required String releaseDate,
    @JsonKey(name: 'artworkUrl100') required String artworkUrl100,
    @JsonKey(name: 'addedDateTime') required DateTime addedDateTime,
  }) = _SavedAlbum;
  factory SavedAlbum.fromJson(Map<String, dynamic> json) => _$SavedAlbumFromJson(json);
}
