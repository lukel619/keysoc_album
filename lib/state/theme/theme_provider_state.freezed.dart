// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'theme_provider_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ThemeProviderState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(ThemeSettings themeSettings) system,
    required TResult Function(ThemeSettings themeSettings) light,
    required TResult Function(ThemeSettings themeSettings) dark,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(ThemeSettings themeSettings)? system,
    TResult Function(ThemeSettings themeSettings)? light,
    TResult Function(ThemeSettings themeSettings)? dark,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(ThemeSettings themeSettings)? system,
    TResult Function(ThemeSettings themeSettings)? light,
    TResult Function(ThemeSettings themeSettings)? dark,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ThemeProviderStateInitial value) initial,
    required TResult Function(_ThemeProviderStateSystem value) system,
    required TResult Function(_ThemeProviderStateLight value) light,
    required TResult Function(_ThemeProviderStateDark value) dark,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ThemeProviderStateInitial value)? initial,
    TResult Function(_ThemeProviderStateSystem value)? system,
    TResult Function(_ThemeProviderStateLight value)? light,
    TResult Function(_ThemeProviderStateDark value)? dark,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ThemeProviderStateInitial value)? initial,
    TResult Function(_ThemeProviderStateSystem value)? system,
    TResult Function(_ThemeProviderStateLight value)? light,
    TResult Function(_ThemeProviderStateDark value)? dark,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ThemeProviderStateCopyWith<$Res> {
  factory $ThemeProviderStateCopyWith(
          ThemeProviderState value, $Res Function(ThemeProviderState) then) =
      _$ThemeProviderStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ThemeProviderStateCopyWithImpl<$Res>
    implements $ThemeProviderStateCopyWith<$Res> {
  _$ThemeProviderStateCopyWithImpl(this._value, this._then);

  final ThemeProviderState _value;
  // ignore: unused_field
  final $Res Function(ThemeProviderState) _then;
}

/// @nodoc
abstract class _$$_ThemeProviderStateInitialCopyWith<$Res> {
  factory _$$_ThemeProviderStateInitialCopyWith(
          _$_ThemeProviderStateInitial value,
          $Res Function(_$_ThemeProviderStateInitial) then) =
      __$$_ThemeProviderStateInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ThemeProviderStateInitialCopyWithImpl<$Res>
    extends _$ThemeProviderStateCopyWithImpl<$Res>
    implements _$$_ThemeProviderStateInitialCopyWith<$Res> {
  __$$_ThemeProviderStateInitialCopyWithImpl(
      _$_ThemeProviderStateInitial _value,
      $Res Function(_$_ThemeProviderStateInitial) _then)
      : super(_value, (v) => _then(v as _$_ThemeProviderStateInitial));

  @override
  _$_ThemeProviderStateInitial get _value =>
      super._value as _$_ThemeProviderStateInitial;
}

/// @nodoc

class _$_ThemeProviderStateInitial implements _ThemeProviderStateInitial {
  const _$_ThemeProviderStateInitial();

  @override
  String toString() {
    return 'ThemeProviderState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ThemeProviderStateInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(ThemeSettings themeSettings) system,
    required TResult Function(ThemeSettings themeSettings) light,
    required TResult Function(ThemeSettings themeSettings) dark,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(ThemeSettings themeSettings)? system,
    TResult Function(ThemeSettings themeSettings)? light,
    TResult Function(ThemeSettings themeSettings)? dark,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(ThemeSettings themeSettings)? system,
    TResult Function(ThemeSettings themeSettings)? light,
    TResult Function(ThemeSettings themeSettings)? dark,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ThemeProviderStateInitial value) initial,
    required TResult Function(_ThemeProviderStateSystem value) system,
    required TResult Function(_ThemeProviderStateLight value) light,
    required TResult Function(_ThemeProviderStateDark value) dark,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ThemeProviderStateInitial value)? initial,
    TResult Function(_ThemeProviderStateSystem value)? system,
    TResult Function(_ThemeProviderStateLight value)? light,
    TResult Function(_ThemeProviderStateDark value)? dark,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ThemeProviderStateInitial value)? initial,
    TResult Function(_ThemeProviderStateSystem value)? system,
    TResult Function(_ThemeProviderStateLight value)? light,
    TResult Function(_ThemeProviderStateDark value)? dark,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _ThemeProviderStateInitial implements ThemeProviderState {
  const factory _ThemeProviderStateInitial() = _$_ThemeProviderStateInitial;
}

/// @nodoc
abstract class _$$_ThemeProviderStateSystemCopyWith<$Res> {
  factory _$$_ThemeProviderStateSystemCopyWith(
          _$_ThemeProviderStateSystem value,
          $Res Function(_$_ThemeProviderStateSystem) then) =
      __$$_ThemeProviderStateSystemCopyWithImpl<$Res>;
  $Res call({ThemeSettings themeSettings});

  $ThemeSettingsCopyWith<$Res> get themeSettings;
}

/// @nodoc
class __$$_ThemeProviderStateSystemCopyWithImpl<$Res>
    extends _$ThemeProviderStateCopyWithImpl<$Res>
    implements _$$_ThemeProviderStateSystemCopyWith<$Res> {
  __$$_ThemeProviderStateSystemCopyWithImpl(_$_ThemeProviderStateSystem _value,
      $Res Function(_$_ThemeProviderStateSystem) _then)
      : super(_value, (v) => _then(v as _$_ThemeProviderStateSystem));

  @override
  _$_ThemeProviderStateSystem get _value =>
      super._value as _$_ThemeProviderStateSystem;

  @override
  $Res call({
    Object? themeSettings = freezed,
  }) {
    return _then(_$_ThemeProviderStateSystem(
      themeSettings: themeSettings == freezed
          ? _value.themeSettings
          : themeSettings // ignore: cast_nullable_to_non_nullable
              as ThemeSettings,
    ));
  }

  @override
  $ThemeSettingsCopyWith<$Res> get themeSettings {
    return $ThemeSettingsCopyWith<$Res>(_value.themeSettings, (value) {
      return _then(_value.copyWith(themeSettings: value));
    });
  }
}

/// @nodoc

class _$_ThemeProviderStateSystem implements _ThemeProviderStateSystem {
  const _$_ThemeProviderStateSystem({required this.themeSettings});

  @override
  final ThemeSettings themeSettings;

  @override
  String toString() {
    return 'ThemeProviderState.system(themeSettings: $themeSettings)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ThemeProviderStateSystem &&
            const DeepCollectionEquality()
                .equals(other.themeSettings, themeSettings));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(themeSettings));

  @JsonKey(ignore: true)
  @override
  _$$_ThemeProviderStateSystemCopyWith<_$_ThemeProviderStateSystem>
      get copyWith => __$$_ThemeProviderStateSystemCopyWithImpl<
          _$_ThemeProviderStateSystem>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(ThemeSettings themeSettings) system,
    required TResult Function(ThemeSettings themeSettings) light,
    required TResult Function(ThemeSettings themeSettings) dark,
  }) {
    return system(themeSettings);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(ThemeSettings themeSettings)? system,
    TResult Function(ThemeSettings themeSettings)? light,
    TResult Function(ThemeSettings themeSettings)? dark,
  }) {
    return system?.call(themeSettings);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(ThemeSettings themeSettings)? system,
    TResult Function(ThemeSettings themeSettings)? light,
    TResult Function(ThemeSettings themeSettings)? dark,
    required TResult orElse(),
  }) {
    if (system != null) {
      return system(themeSettings);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ThemeProviderStateInitial value) initial,
    required TResult Function(_ThemeProviderStateSystem value) system,
    required TResult Function(_ThemeProviderStateLight value) light,
    required TResult Function(_ThemeProviderStateDark value) dark,
  }) {
    return system(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ThemeProviderStateInitial value)? initial,
    TResult Function(_ThemeProviderStateSystem value)? system,
    TResult Function(_ThemeProviderStateLight value)? light,
    TResult Function(_ThemeProviderStateDark value)? dark,
  }) {
    return system?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ThemeProviderStateInitial value)? initial,
    TResult Function(_ThemeProviderStateSystem value)? system,
    TResult Function(_ThemeProviderStateLight value)? light,
    TResult Function(_ThemeProviderStateDark value)? dark,
    required TResult orElse(),
  }) {
    if (system != null) {
      return system(this);
    }
    return orElse();
  }
}

abstract class _ThemeProviderStateSystem implements ThemeProviderState {
  const factory _ThemeProviderStateSystem(
          {required final ThemeSettings themeSettings}) =
      _$_ThemeProviderStateSystem;

  ThemeSettings get themeSettings => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_ThemeProviderStateSystemCopyWith<_$_ThemeProviderStateSystem>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ThemeProviderStateLightCopyWith<$Res> {
  factory _$$_ThemeProviderStateLightCopyWith(_$_ThemeProviderStateLight value,
          $Res Function(_$_ThemeProviderStateLight) then) =
      __$$_ThemeProviderStateLightCopyWithImpl<$Res>;
  $Res call({ThemeSettings themeSettings});

  $ThemeSettingsCopyWith<$Res> get themeSettings;
}

/// @nodoc
class __$$_ThemeProviderStateLightCopyWithImpl<$Res>
    extends _$ThemeProviderStateCopyWithImpl<$Res>
    implements _$$_ThemeProviderStateLightCopyWith<$Res> {
  __$$_ThemeProviderStateLightCopyWithImpl(_$_ThemeProviderStateLight _value,
      $Res Function(_$_ThemeProviderStateLight) _then)
      : super(_value, (v) => _then(v as _$_ThemeProviderStateLight));

  @override
  _$_ThemeProviderStateLight get _value =>
      super._value as _$_ThemeProviderStateLight;

  @override
  $Res call({
    Object? themeSettings = freezed,
  }) {
    return _then(_$_ThemeProviderStateLight(
      themeSettings: themeSettings == freezed
          ? _value.themeSettings
          : themeSettings // ignore: cast_nullable_to_non_nullable
              as ThemeSettings,
    ));
  }

  @override
  $ThemeSettingsCopyWith<$Res> get themeSettings {
    return $ThemeSettingsCopyWith<$Res>(_value.themeSettings, (value) {
      return _then(_value.copyWith(themeSettings: value));
    });
  }
}

/// @nodoc

class _$_ThemeProviderStateLight implements _ThemeProviderStateLight {
  const _$_ThemeProviderStateLight({required this.themeSettings});

  @override
  final ThemeSettings themeSettings;

  @override
  String toString() {
    return 'ThemeProviderState.light(themeSettings: $themeSettings)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ThemeProviderStateLight &&
            const DeepCollectionEquality()
                .equals(other.themeSettings, themeSettings));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(themeSettings));

  @JsonKey(ignore: true)
  @override
  _$$_ThemeProviderStateLightCopyWith<_$_ThemeProviderStateLight>
      get copyWith =>
          __$$_ThemeProviderStateLightCopyWithImpl<_$_ThemeProviderStateLight>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(ThemeSettings themeSettings) system,
    required TResult Function(ThemeSettings themeSettings) light,
    required TResult Function(ThemeSettings themeSettings) dark,
  }) {
    return light(themeSettings);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(ThemeSettings themeSettings)? system,
    TResult Function(ThemeSettings themeSettings)? light,
    TResult Function(ThemeSettings themeSettings)? dark,
  }) {
    return light?.call(themeSettings);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(ThemeSettings themeSettings)? system,
    TResult Function(ThemeSettings themeSettings)? light,
    TResult Function(ThemeSettings themeSettings)? dark,
    required TResult orElse(),
  }) {
    if (light != null) {
      return light(themeSettings);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ThemeProviderStateInitial value) initial,
    required TResult Function(_ThemeProviderStateSystem value) system,
    required TResult Function(_ThemeProviderStateLight value) light,
    required TResult Function(_ThemeProviderStateDark value) dark,
  }) {
    return light(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ThemeProviderStateInitial value)? initial,
    TResult Function(_ThemeProviderStateSystem value)? system,
    TResult Function(_ThemeProviderStateLight value)? light,
    TResult Function(_ThemeProviderStateDark value)? dark,
  }) {
    return light?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ThemeProviderStateInitial value)? initial,
    TResult Function(_ThemeProviderStateSystem value)? system,
    TResult Function(_ThemeProviderStateLight value)? light,
    TResult Function(_ThemeProviderStateDark value)? dark,
    required TResult orElse(),
  }) {
    if (light != null) {
      return light(this);
    }
    return orElse();
  }
}

abstract class _ThemeProviderStateLight implements ThemeProviderState {
  const factory _ThemeProviderStateLight(
          {required final ThemeSettings themeSettings}) =
      _$_ThemeProviderStateLight;

  ThemeSettings get themeSettings => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_ThemeProviderStateLightCopyWith<_$_ThemeProviderStateLight>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ThemeProviderStateDarkCopyWith<$Res> {
  factory _$$_ThemeProviderStateDarkCopyWith(_$_ThemeProviderStateDark value,
          $Res Function(_$_ThemeProviderStateDark) then) =
      __$$_ThemeProviderStateDarkCopyWithImpl<$Res>;
  $Res call({ThemeSettings themeSettings});

  $ThemeSettingsCopyWith<$Res> get themeSettings;
}

/// @nodoc
class __$$_ThemeProviderStateDarkCopyWithImpl<$Res>
    extends _$ThemeProviderStateCopyWithImpl<$Res>
    implements _$$_ThemeProviderStateDarkCopyWith<$Res> {
  __$$_ThemeProviderStateDarkCopyWithImpl(_$_ThemeProviderStateDark _value,
      $Res Function(_$_ThemeProviderStateDark) _then)
      : super(_value, (v) => _then(v as _$_ThemeProviderStateDark));

  @override
  _$_ThemeProviderStateDark get _value =>
      super._value as _$_ThemeProviderStateDark;

  @override
  $Res call({
    Object? themeSettings = freezed,
  }) {
    return _then(_$_ThemeProviderStateDark(
      themeSettings: themeSettings == freezed
          ? _value.themeSettings
          : themeSettings // ignore: cast_nullable_to_non_nullable
              as ThemeSettings,
    ));
  }

  @override
  $ThemeSettingsCopyWith<$Res> get themeSettings {
    return $ThemeSettingsCopyWith<$Res>(_value.themeSettings, (value) {
      return _then(_value.copyWith(themeSettings: value));
    });
  }
}

/// @nodoc

class _$_ThemeProviderStateDark implements _ThemeProviderStateDark {
  const _$_ThemeProviderStateDark({required this.themeSettings});

  @override
  final ThemeSettings themeSettings;

  @override
  String toString() {
    return 'ThemeProviderState.dark(themeSettings: $themeSettings)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ThemeProviderStateDark &&
            const DeepCollectionEquality()
                .equals(other.themeSettings, themeSettings));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(themeSettings));

  @JsonKey(ignore: true)
  @override
  _$$_ThemeProviderStateDarkCopyWith<_$_ThemeProviderStateDark> get copyWith =>
      __$$_ThemeProviderStateDarkCopyWithImpl<_$_ThemeProviderStateDark>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(ThemeSettings themeSettings) system,
    required TResult Function(ThemeSettings themeSettings) light,
    required TResult Function(ThemeSettings themeSettings) dark,
  }) {
    return dark(themeSettings);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(ThemeSettings themeSettings)? system,
    TResult Function(ThemeSettings themeSettings)? light,
    TResult Function(ThemeSettings themeSettings)? dark,
  }) {
    return dark?.call(themeSettings);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(ThemeSettings themeSettings)? system,
    TResult Function(ThemeSettings themeSettings)? light,
    TResult Function(ThemeSettings themeSettings)? dark,
    required TResult orElse(),
  }) {
    if (dark != null) {
      return dark(themeSettings);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ThemeProviderStateInitial value) initial,
    required TResult Function(_ThemeProviderStateSystem value) system,
    required TResult Function(_ThemeProviderStateLight value) light,
    required TResult Function(_ThemeProviderStateDark value) dark,
  }) {
    return dark(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ThemeProviderStateInitial value)? initial,
    TResult Function(_ThemeProviderStateSystem value)? system,
    TResult Function(_ThemeProviderStateLight value)? light,
    TResult Function(_ThemeProviderStateDark value)? dark,
  }) {
    return dark?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ThemeProviderStateInitial value)? initial,
    TResult Function(_ThemeProviderStateSystem value)? system,
    TResult Function(_ThemeProviderStateLight value)? light,
    TResult Function(_ThemeProviderStateDark value)? dark,
    required TResult orElse(),
  }) {
    if (dark != null) {
      return dark(this);
    }
    return orElse();
  }
}

abstract class _ThemeProviderStateDark implements ThemeProviderState {
  const factory _ThemeProviderStateDark(
      {required final ThemeSettings themeSettings}) = _$_ThemeProviderStateDark;

  ThemeSettings get themeSettings => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_ThemeProviderStateDarkCopyWith<_$_ThemeProviderStateDark> get copyWith =>
      throw _privateConstructorUsedError;
}
