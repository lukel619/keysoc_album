import 'package:album/model/theme/theme_settings.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'theme_provider_state.freezed.dart';

@freezed
class ThemeProviderState with _$ThemeProviderState {
  const factory ThemeProviderState.initial() = _ThemeProviderStateInitial;
  const factory ThemeProviderState.system({required ThemeSettings themeSettings}) = _ThemeProviderStateSystem;
  const factory ThemeProviderState.light({required ThemeSettings themeSettings}) = _ThemeProviderStateLight;
  const factory ThemeProviderState.dark({required ThemeSettings themeSettings}) = _ThemeProviderStateDark;
}
