import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_provider_state.freezed.dart';

@freezed
class HomeProviderState with _$HomeProviderState {
  const factory HomeProviderState.initial() = _HomeProviderStateInitial;
  const factory HomeProviderState.data({required int selectedIndex}) = _HomeProviderStateData;
}
