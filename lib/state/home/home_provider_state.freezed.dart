// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'home_provider_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomeProviderState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(int selectedIndex) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(int selectedIndex)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(int selectedIndex)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_HomeProviderStateInitial value) initial,
    required TResult Function(_HomeProviderStateData value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_HomeProviderStateInitial value)? initial,
    TResult Function(_HomeProviderStateData value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_HomeProviderStateInitial value)? initial,
    TResult Function(_HomeProviderStateData value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeProviderStateCopyWith<$Res> {
  factory $HomeProviderStateCopyWith(
          HomeProviderState value, $Res Function(HomeProviderState) then) =
      _$HomeProviderStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomeProviderStateCopyWithImpl<$Res>
    implements $HomeProviderStateCopyWith<$Res> {
  _$HomeProviderStateCopyWithImpl(this._value, this._then);

  final HomeProviderState _value;
  // ignore: unused_field
  final $Res Function(HomeProviderState) _then;
}

/// @nodoc
abstract class _$$_HomeProviderStateInitialCopyWith<$Res> {
  factory _$$_HomeProviderStateInitialCopyWith(
          _$_HomeProviderStateInitial value,
          $Res Function(_$_HomeProviderStateInitial) then) =
      __$$_HomeProviderStateInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_HomeProviderStateInitialCopyWithImpl<$Res>
    extends _$HomeProviderStateCopyWithImpl<$Res>
    implements _$$_HomeProviderStateInitialCopyWith<$Res> {
  __$$_HomeProviderStateInitialCopyWithImpl(_$_HomeProviderStateInitial _value,
      $Res Function(_$_HomeProviderStateInitial) _then)
      : super(_value, (v) => _then(v as _$_HomeProviderStateInitial));

  @override
  _$_HomeProviderStateInitial get _value =>
      super._value as _$_HomeProviderStateInitial;
}

/// @nodoc

class _$_HomeProviderStateInitial implements _HomeProviderStateInitial {
  const _$_HomeProviderStateInitial();

  @override
  String toString() {
    return 'HomeProviderState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_HomeProviderStateInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(int selectedIndex) data,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(int selectedIndex)? data,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(int selectedIndex)? data,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_HomeProviderStateInitial value) initial,
    required TResult Function(_HomeProviderStateData value) data,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_HomeProviderStateInitial value)? initial,
    TResult Function(_HomeProviderStateData value)? data,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_HomeProviderStateInitial value)? initial,
    TResult Function(_HomeProviderStateData value)? data,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _HomeProviderStateInitial implements HomeProviderState {
  const factory _HomeProviderStateInitial() = _$_HomeProviderStateInitial;
}

/// @nodoc
abstract class _$$_HomeProviderStateDataCopyWith<$Res> {
  factory _$$_HomeProviderStateDataCopyWith(_$_HomeProviderStateData value,
          $Res Function(_$_HomeProviderStateData) then) =
      __$$_HomeProviderStateDataCopyWithImpl<$Res>;
  $Res call({int selectedIndex});
}

/// @nodoc
class __$$_HomeProviderStateDataCopyWithImpl<$Res>
    extends _$HomeProviderStateCopyWithImpl<$Res>
    implements _$$_HomeProviderStateDataCopyWith<$Res> {
  __$$_HomeProviderStateDataCopyWithImpl(_$_HomeProviderStateData _value,
      $Res Function(_$_HomeProviderStateData) _then)
      : super(_value, (v) => _then(v as _$_HomeProviderStateData));

  @override
  _$_HomeProviderStateData get _value =>
      super._value as _$_HomeProviderStateData;

  @override
  $Res call({
    Object? selectedIndex = freezed,
  }) {
    return _then(_$_HomeProviderStateData(
      selectedIndex: selectedIndex == freezed
          ? _value.selectedIndex
          : selectedIndex // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_HomeProviderStateData implements _HomeProviderStateData {
  const _$_HomeProviderStateData({required this.selectedIndex});

  @override
  final int selectedIndex;

  @override
  String toString() {
    return 'HomeProviderState.data(selectedIndex: $selectedIndex)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_HomeProviderStateData &&
            const DeepCollectionEquality()
                .equals(other.selectedIndex, selectedIndex));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(selectedIndex));

  @JsonKey(ignore: true)
  @override
  _$$_HomeProviderStateDataCopyWith<_$_HomeProviderStateData> get copyWith =>
      __$$_HomeProviderStateDataCopyWithImpl<_$_HomeProviderStateData>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(int selectedIndex) data,
  }) {
    return data(selectedIndex);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(int selectedIndex)? data,
  }) {
    return data?.call(selectedIndex);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(int selectedIndex)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(selectedIndex);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_HomeProviderStateInitial value) initial,
    required TResult Function(_HomeProviderStateData value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_HomeProviderStateInitial value)? initial,
    TResult Function(_HomeProviderStateData value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_HomeProviderStateInitial value)? initial,
    TResult Function(_HomeProviderStateData value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class _HomeProviderStateData implements HomeProviderState {
  const factory _HomeProviderStateData({required final int selectedIndex}) =
      _$_HomeProviderStateData;

  int get selectedIndex => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_HomeProviderStateDataCopyWith<_$_HomeProviderStateData> get copyWith =>
      throw _privateConstructorUsedError;
}
