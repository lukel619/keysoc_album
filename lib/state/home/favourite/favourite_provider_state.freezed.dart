// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'favourite_provider_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FavouriteProviderState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(List<SavedAlbum>? savedAlbumList) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<SavedAlbum>? savedAlbumList)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<SavedAlbum>? savedAlbumList)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_FavouriteProviderStateInitial value) initial,
    required TResult Function(_FavouriteProviderStateData value) data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_FavouriteProviderStateInitial value)? initial,
    TResult Function(_FavouriteProviderStateData value)? data,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_FavouriteProviderStateInitial value)? initial,
    TResult Function(_FavouriteProviderStateData value)? data,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FavouriteProviderStateCopyWith<$Res> {
  factory $FavouriteProviderStateCopyWith(FavouriteProviderState value,
          $Res Function(FavouriteProviderState) then) =
      _$FavouriteProviderStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$FavouriteProviderStateCopyWithImpl<$Res>
    implements $FavouriteProviderStateCopyWith<$Res> {
  _$FavouriteProviderStateCopyWithImpl(this._value, this._then);

  final FavouriteProviderState _value;
  // ignore: unused_field
  final $Res Function(FavouriteProviderState) _then;
}

/// @nodoc
abstract class _$$_FavouriteProviderStateInitialCopyWith<$Res> {
  factory _$$_FavouriteProviderStateInitialCopyWith(
          _$_FavouriteProviderStateInitial value,
          $Res Function(_$_FavouriteProviderStateInitial) then) =
      __$$_FavouriteProviderStateInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_FavouriteProviderStateInitialCopyWithImpl<$Res>
    extends _$FavouriteProviderStateCopyWithImpl<$Res>
    implements _$$_FavouriteProviderStateInitialCopyWith<$Res> {
  __$$_FavouriteProviderStateInitialCopyWithImpl(
      _$_FavouriteProviderStateInitial _value,
      $Res Function(_$_FavouriteProviderStateInitial) _then)
      : super(_value, (v) => _then(v as _$_FavouriteProviderStateInitial));

  @override
  _$_FavouriteProviderStateInitial get _value =>
      super._value as _$_FavouriteProviderStateInitial;
}

/// @nodoc

class _$_FavouriteProviderStateInitial
    implements _FavouriteProviderStateInitial {
  const _$_FavouriteProviderStateInitial();

  @override
  String toString() {
    return 'FavouriteProviderState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FavouriteProviderStateInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(List<SavedAlbum>? savedAlbumList) data,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<SavedAlbum>? savedAlbumList)? data,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<SavedAlbum>? savedAlbumList)? data,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_FavouriteProviderStateInitial value) initial,
    required TResult Function(_FavouriteProviderStateData value) data,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_FavouriteProviderStateInitial value)? initial,
    TResult Function(_FavouriteProviderStateData value)? data,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_FavouriteProviderStateInitial value)? initial,
    TResult Function(_FavouriteProviderStateData value)? data,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _FavouriteProviderStateInitial
    implements FavouriteProviderState {
  const factory _FavouriteProviderStateInitial() =
      _$_FavouriteProviderStateInitial;
}

/// @nodoc
abstract class _$$_FavouriteProviderStateDataCopyWith<$Res> {
  factory _$$_FavouriteProviderStateDataCopyWith(
          _$_FavouriteProviderStateData value,
          $Res Function(_$_FavouriteProviderStateData) then) =
      __$$_FavouriteProviderStateDataCopyWithImpl<$Res>;
  $Res call({List<SavedAlbum>? savedAlbumList});
}

/// @nodoc
class __$$_FavouriteProviderStateDataCopyWithImpl<$Res>
    extends _$FavouriteProviderStateCopyWithImpl<$Res>
    implements _$$_FavouriteProviderStateDataCopyWith<$Res> {
  __$$_FavouriteProviderStateDataCopyWithImpl(
      _$_FavouriteProviderStateData _value,
      $Res Function(_$_FavouriteProviderStateData) _then)
      : super(_value, (v) => _then(v as _$_FavouriteProviderStateData));

  @override
  _$_FavouriteProviderStateData get _value =>
      super._value as _$_FavouriteProviderStateData;

  @override
  $Res call({
    Object? savedAlbumList = freezed,
  }) {
    return _then(_$_FavouriteProviderStateData(
      savedAlbumList: savedAlbumList == freezed
          ? _value._savedAlbumList
          : savedAlbumList // ignore: cast_nullable_to_non_nullable
              as List<SavedAlbum>?,
    ));
  }
}

/// @nodoc

class _$_FavouriteProviderStateData implements _FavouriteProviderStateData {
  const _$_FavouriteProviderStateData({final List<SavedAlbum>? savedAlbumList})
      : _savedAlbumList = savedAlbumList;

  final List<SavedAlbum>? _savedAlbumList;
  @override
  List<SavedAlbum>? get savedAlbumList {
    final value = _savedAlbumList;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'FavouriteProviderState.data(savedAlbumList: $savedAlbumList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FavouriteProviderStateData &&
            const DeepCollectionEquality()
                .equals(other._savedAlbumList, _savedAlbumList));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_savedAlbumList));

  @JsonKey(ignore: true)
  @override
  _$$_FavouriteProviderStateDataCopyWith<_$_FavouriteProviderStateData>
      get copyWith => __$$_FavouriteProviderStateDataCopyWithImpl<
          _$_FavouriteProviderStateData>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(List<SavedAlbum>? savedAlbumList) data,
  }) {
    return data(savedAlbumList);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<SavedAlbum>? savedAlbumList)? data,
  }) {
    return data?.call(savedAlbumList);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(List<SavedAlbum>? savedAlbumList)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(savedAlbumList);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_FavouriteProviderStateInitial value) initial,
    required TResult Function(_FavouriteProviderStateData value) data,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_FavouriteProviderStateInitial value)? initial,
    TResult Function(_FavouriteProviderStateData value)? data,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_FavouriteProviderStateInitial value)? initial,
    TResult Function(_FavouriteProviderStateData value)? data,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class _FavouriteProviderStateData implements FavouriteProviderState {
  const factory _FavouriteProviderStateData(
      {final List<SavedAlbum>? savedAlbumList}) = _$_FavouriteProviderStateData;

  List<SavedAlbum>? get savedAlbumList => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_FavouriteProviderStateDataCopyWith<_$_FavouriteProviderStateData>
      get copyWith => throw _privateConstructorUsedError;
}
