import 'package:album/model/home/favourite/saved_album.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'favourite_provider_state.freezed.dart';

@freezed
class FavouriteProviderState with _$FavouriteProviderState {
  const factory FavouriteProviderState.initial() = _FavouriteProviderStateInitial;
  const factory FavouriteProviderState.data({List<SavedAlbum>? savedAlbumList}) = _FavouriteProviderStateData;
}
