// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'explore_provider_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ExploreProviderState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(AlbumList list, String? profileImageLink) data,
    required TResult Function() error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(AlbumList list, String? profileImageLink)? data,
    TResult Function()? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(AlbumList list, String? profileImageLink)? data,
    TResult Function()? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ExploreProviderStateInitial value) initial,
    required TResult Function(_ExploreProviderStateLoading value) loading,
    required TResult Function(_ExploreProviderStateData value) data,
    required TResult Function(_ExploreProviderStateError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ExploreProviderStateInitial value)? initial,
    TResult Function(_ExploreProviderStateLoading value)? loading,
    TResult Function(_ExploreProviderStateData value)? data,
    TResult Function(_ExploreProviderStateError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ExploreProviderStateInitial value)? initial,
    TResult Function(_ExploreProviderStateLoading value)? loading,
    TResult Function(_ExploreProviderStateData value)? data,
    TResult Function(_ExploreProviderStateError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExploreProviderStateCopyWith<$Res> {
  factory $ExploreProviderStateCopyWith(ExploreProviderState value,
          $Res Function(ExploreProviderState) then) =
      _$ExploreProviderStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ExploreProviderStateCopyWithImpl<$Res>
    implements $ExploreProviderStateCopyWith<$Res> {
  _$ExploreProviderStateCopyWithImpl(this._value, this._then);

  final ExploreProviderState _value;
  // ignore: unused_field
  final $Res Function(ExploreProviderState) _then;
}

/// @nodoc
abstract class _$$_ExploreProviderStateInitialCopyWith<$Res> {
  factory _$$_ExploreProviderStateInitialCopyWith(
          _$_ExploreProviderStateInitial value,
          $Res Function(_$_ExploreProviderStateInitial) then) =
      __$$_ExploreProviderStateInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ExploreProviderStateInitialCopyWithImpl<$Res>
    extends _$ExploreProviderStateCopyWithImpl<$Res>
    implements _$$_ExploreProviderStateInitialCopyWith<$Res> {
  __$$_ExploreProviderStateInitialCopyWithImpl(
      _$_ExploreProviderStateInitial _value,
      $Res Function(_$_ExploreProviderStateInitial) _then)
      : super(_value, (v) => _then(v as _$_ExploreProviderStateInitial));

  @override
  _$_ExploreProviderStateInitial get _value =>
      super._value as _$_ExploreProviderStateInitial;
}

/// @nodoc

class _$_ExploreProviderStateInitial implements _ExploreProviderStateInitial {
  const _$_ExploreProviderStateInitial();

  @override
  String toString() {
    return 'ExploreProviderState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ExploreProviderStateInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(AlbumList list, String? profileImageLink) data,
    required TResult Function() error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(AlbumList list, String? profileImageLink)? data,
    TResult Function()? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(AlbumList list, String? profileImageLink)? data,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ExploreProviderStateInitial value) initial,
    required TResult Function(_ExploreProviderStateLoading value) loading,
    required TResult Function(_ExploreProviderStateData value) data,
    required TResult Function(_ExploreProviderStateError value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ExploreProviderStateInitial value)? initial,
    TResult Function(_ExploreProviderStateLoading value)? loading,
    TResult Function(_ExploreProviderStateData value)? data,
    TResult Function(_ExploreProviderStateError value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ExploreProviderStateInitial value)? initial,
    TResult Function(_ExploreProviderStateLoading value)? loading,
    TResult Function(_ExploreProviderStateData value)? data,
    TResult Function(_ExploreProviderStateError value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _ExploreProviderStateInitial implements ExploreProviderState {
  const factory _ExploreProviderStateInitial() = _$_ExploreProviderStateInitial;
}

/// @nodoc
abstract class _$$_ExploreProviderStateLoadingCopyWith<$Res> {
  factory _$$_ExploreProviderStateLoadingCopyWith(
          _$_ExploreProviderStateLoading value,
          $Res Function(_$_ExploreProviderStateLoading) then) =
      __$$_ExploreProviderStateLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ExploreProviderStateLoadingCopyWithImpl<$Res>
    extends _$ExploreProviderStateCopyWithImpl<$Res>
    implements _$$_ExploreProviderStateLoadingCopyWith<$Res> {
  __$$_ExploreProviderStateLoadingCopyWithImpl(
      _$_ExploreProviderStateLoading _value,
      $Res Function(_$_ExploreProviderStateLoading) _then)
      : super(_value, (v) => _then(v as _$_ExploreProviderStateLoading));

  @override
  _$_ExploreProviderStateLoading get _value =>
      super._value as _$_ExploreProviderStateLoading;
}

/// @nodoc

class _$_ExploreProviderStateLoading implements _ExploreProviderStateLoading {
  const _$_ExploreProviderStateLoading();

  @override
  String toString() {
    return 'ExploreProviderState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ExploreProviderStateLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(AlbumList list, String? profileImageLink) data,
    required TResult Function() error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(AlbumList list, String? profileImageLink)? data,
    TResult Function()? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(AlbumList list, String? profileImageLink)? data,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ExploreProviderStateInitial value) initial,
    required TResult Function(_ExploreProviderStateLoading value) loading,
    required TResult Function(_ExploreProviderStateData value) data,
    required TResult Function(_ExploreProviderStateError value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ExploreProviderStateInitial value)? initial,
    TResult Function(_ExploreProviderStateLoading value)? loading,
    TResult Function(_ExploreProviderStateData value)? data,
    TResult Function(_ExploreProviderStateError value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ExploreProviderStateInitial value)? initial,
    TResult Function(_ExploreProviderStateLoading value)? loading,
    TResult Function(_ExploreProviderStateData value)? data,
    TResult Function(_ExploreProviderStateError value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _ExploreProviderStateLoading implements ExploreProviderState {
  const factory _ExploreProviderStateLoading() = _$_ExploreProviderStateLoading;
}

/// @nodoc
abstract class _$$_ExploreProviderStateDataCopyWith<$Res> {
  factory _$$_ExploreProviderStateDataCopyWith(
          _$_ExploreProviderStateData value,
          $Res Function(_$_ExploreProviderStateData) then) =
      __$$_ExploreProviderStateDataCopyWithImpl<$Res>;
  $Res call({AlbumList list, String? profileImageLink});

  $AlbumListCopyWith<$Res> get list;
}

/// @nodoc
class __$$_ExploreProviderStateDataCopyWithImpl<$Res>
    extends _$ExploreProviderStateCopyWithImpl<$Res>
    implements _$$_ExploreProviderStateDataCopyWith<$Res> {
  __$$_ExploreProviderStateDataCopyWithImpl(_$_ExploreProviderStateData _value,
      $Res Function(_$_ExploreProviderStateData) _then)
      : super(_value, (v) => _then(v as _$_ExploreProviderStateData));

  @override
  _$_ExploreProviderStateData get _value =>
      super._value as _$_ExploreProviderStateData;

  @override
  $Res call({
    Object? list = freezed,
    Object? profileImageLink = freezed,
  }) {
    return _then(_$_ExploreProviderStateData(
      list: list == freezed
          ? _value.list
          : list // ignore: cast_nullable_to_non_nullable
              as AlbumList,
      profileImageLink: profileImageLink == freezed
          ? _value.profileImageLink
          : profileImageLink // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $AlbumListCopyWith<$Res> get list {
    return $AlbumListCopyWith<$Res>(_value.list, (value) {
      return _then(_value.copyWith(list: value));
    });
  }
}

/// @nodoc

class _$_ExploreProviderStateData implements _ExploreProviderStateData {
  const _$_ExploreProviderStateData(
      {required this.list, this.profileImageLink});

  @override
  final AlbumList list;
  @override
  final String? profileImageLink;

  @override
  String toString() {
    return 'ExploreProviderState.data(list: $list, profileImageLink: $profileImageLink)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ExploreProviderStateData &&
            const DeepCollectionEquality().equals(other.list, list) &&
            const DeepCollectionEquality()
                .equals(other.profileImageLink, profileImageLink));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(list),
      const DeepCollectionEquality().hash(profileImageLink));

  @JsonKey(ignore: true)
  @override
  _$$_ExploreProviderStateDataCopyWith<_$_ExploreProviderStateData>
      get copyWith => __$$_ExploreProviderStateDataCopyWithImpl<
          _$_ExploreProviderStateData>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(AlbumList list, String? profileImageLink) data,
    required TResult Function() error,
  }) {
    return data(list, profileImageLink);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(AlbumList list, String? profileImageLink)? data,
    TResult Function()? error,
  }) {
    return data?.call(list, profileImageLink);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(AlbumList list, String? profileImageLink)? data,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(list, profileImageLink);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ExploreProviderStateInitial value) initial,
    required TResult Function(_ExploreProviderStateLoading value) loading,
    required TResult Function(_ExploreProviderStateData value) data,
    required TResult Function(_ExploreProviderStateError value) error,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ExploreProviderStateInitial value)? initial,
    TResult Function(_ExploreProviderStateLoading value)? loading,
    TResult Function(_ExploreProviderStateData value)? data,
    TResult Function(_ExploreProviderStateError value)? error,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ExploreProviderStateInitial value)? initial,
    TResult Function(_ExploreProviderStateLoading value)? loading,
    TResult Function(_ExploreProviderStateData value)? data,
    TResult Function(_ExploreProviderStateError value)? error,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class _ExploreProviderStateData implements ExploreProviderState {
  const factory _ExploreProviderStateData(
      {required final AlbumList list,
      final String? profileImageLink}) = _$_ExploreProviderStateData;

  AlbumList get list => throw _privateConstructorUsedError;
  String? get profileImageLink => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$_ExploreProviderStateDataCopyWith<_$_ExploreProviderStateData>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ExploreProviderStateErrorCopyWith<$Res> {
  factory _$$_ExploreProviderStateErrorCopyWith(
          _$_ExploreProviderStateError value,
          $Res Function(_$_ExploreProviderStateError) then) =
      __$$_ExploreProviderStateErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ExploreProviderStateErrorCopyWithImpl<$Res>
    extends _$ExploreProviderStateCopyWithImpl<$Res>
    implements _$$_ExploreProviderStateErrorCopyWith<$Res> {
  __$$_ExploreProviderStateErrorCopyWithImpl(
      _$_ExploreProviderStateError _value,
      $Res Function(_$_ExploreProviderStateError) _then)
      : super(_value, (v) => _then(v as _$_ExploreProviderStateError));

  @override
  _$_ExploreProviderStateError get _value =>
      super._value as _$_ExploreProviderStateError;
}

/// @nodoc

class _$_ExploreProviderStateError implements _ExploreProviderStateError {
  const _$_ExploreProviderStateError();

  @override
  String toString() {
    return 'ExploreProviderState.error()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ExploreProviderStateError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(AlbumList list, String? profileImageLink) data,
    required TResult Function() error,
  }) {
    return error();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(AlbumList list, String? profileImageLink)? data,
    TResult Function()? error,
  }) {
    return error?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(AlbumList list, String? profileImageLink)? data,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ExploreProviderStateInitial value) initial,
    required TResult Function(_ExploreProviderStateLoading value) loading,
    required TResult Function(_ExploreProviderStateData value) data,
    required TResult Function(_ExploreProviderStateError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_ExploreProviderStateInitial value)? initial,
    TResult Function(_ExploreProviderStateLoading value)? loading,
    TResult Function(_ExploreProviderStateData value)? data,
    TResult Function(_ExploreProviderStateError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ExploreProviderStateInitial value)? initial,
    TResult Function(_ExploreProviderStateLoading value)? loading,
    TResult Function(_ExploreProviderStateData value)? data,
    TResult Function(_ExploreProviderStateError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _ExploreProviderStateError implements ExploreProviderState {
  const factory _ExploreProviderStateError() = _$_ExploreProviderStateError;
}
