import 'package:album/model/home/explore/album_list.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'explore_provider_state.freezed.dart';

@freezed
class ExploreProviderState with _$ExploreProviderState {
  const factory ExploreProviderState.initial() = _ExploreProviderStateInitial;
  const factory ExploreProviderState.loading() = _ExploreProviderStateLoading;
  const factory ExploreProviderState.data({required AlbumList list, String? profileImageLink}) = _ExploreProviderStateData;
  const factory ExploreProviderState.error() = _ExploreProviderStateError;
}
