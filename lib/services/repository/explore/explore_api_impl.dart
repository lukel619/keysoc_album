import 'dart:async';
import 'dart:convert';

import 'package:album/model/home/explore/album_list.dart';
import 'package:album/provider_locator.dart';
import 'package:album/services/api_client.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

abstract class ExploreService {
  Future<AlbumList> getAlbumList({required String termString});
  Future<String?> getArtistArtworkUrl(int artistId);
}

class ExploreAPIImpl extends ExploreService {
  ExploreAPIImpl(this.read);

  final Reader read;
  late final ApiClient _apiClientProvider = read(ProviderLocator.apiClientProvider);

  @override
  Future<AlbumList> getAlbumList({required String termString}) async {
    final Response res = await _apiClientProvider.client.get('/search?term=$termString&entity=album');
    if (res.statusCode == 200) {
      final Map<String, dynamic> jsonResponse = jsonDecode(res.data);
      final AlbumList albumList = AlbumList.fromJson(jsonResponse);
      return albumList;
    }

    throw Exception();
  }

  // This api will take longer loading time
  @override
  Future<String?> getArtistArtworkUrl(int artistId) async {
    try {
      int width = 2400;
      int height = 933;
      final Response response = await _apiClientProvider.client.get(
        'https://music.apple.com/de/artist/$artistId',
        options: Options(contentType: 'text/html; charset=utf-8'),
      );
      final document = response.data;

      RegExp regEx = RegExp("<meta property=\"og:image\" content=\"(.*png)\"");
      RegExpMatch? match = regEx.firstMatch(document);

      if (match != null) {
        String rawImageUrl = match.group(1) ?? '';
        String imageUrl = rawImageUrl.replaceAll(RegExp(r'[\d]+x[\d]+(cw)+'), '${width}x${height}cc');
        return imageUrl;
      }

      return null;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }
}
