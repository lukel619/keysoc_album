import 'package:dio/dio.dart';

class ApiClient {
  ApiClient();

  // only a single instance of client will be constructed
  static Dio? _client;

  Dio get client {
    if (_client == null) {
      // ? maybe use dotEnv for controlling the base url
      BaseOptions options = BaseOptions(
        baseUrl: 'https://itunes.apple.com',
        connectTimeout: 5000,
        receiveTimeout: 5000,
        contentType: 'application/json',
      );
      return _client = Dio(options);
    } else {
      return _client!;
    }
  }
}
