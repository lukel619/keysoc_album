# Keysoc Album

A Demo Album Application

## Compile Dependencies
- Flutter >= 3.0.0

## Tested Features and Package in this project
- [x] Material 3 
- [x] Flutter Native Splash Screen
- [x] Flutter Neumorphic
- [x] Dio